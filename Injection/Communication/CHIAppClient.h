//
//  CHIAppClient.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHIAppCommunication.h"

@interface CHIAppClient : CHIAppCommunication 

+ (instancetype)sharedInstance;

@end
