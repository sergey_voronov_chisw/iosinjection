//
//  CHIInjectedAppManager.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIInjectedAppManager.h"
#import "CHIWindowAnalyzer.h"
#import "CHIAppClient.h"
#import "Constants.h"
#import <objc/runtime.h>
#import "NSData+Base64.h"
#import "NSBundle+Fonts.h"

static CHIWindowAnalyzer *_analyzer;
static NSString *_projectPath;
static BOOL _enable;
static BOOL _inSandbox;
static NSString *_fontsServerDirectoryPath;
static NSArray<NSURL *> *_fontsLocalPathes;

@implementation CHIInjectedAppManager

+ (void)initialize {
    _fontsLocalPathes = [[NSBundle mainBundle] fonts];
}

#pragma mark - Public methods

+ (void)start {
    
    NSLog(@"===== %s", __FUNCTION__);
    
    if (!_analyzer) {
        
        [CHIAppClient sharedInstance];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onClientNotification:)
                                                     name:kClientNotification
                                                   object:nil];
        _analyzer = [[CHIWindowAnalyzer alloc] init];
        _enable = YES;
    }
}

+ (void)onClientNotification:(id)notification {
    
//    NSLog(@"===== %s:\n\n%@\n\n ", __FUNCTION__, [notification userInfo]);
    
    NSDictionary *userInfoRequest = [notification userInfo][kNotificationUserInfoKeyRequest];

    NSString *type = userInfoRequest[@"type"];
    NSString *name = userInfoRequest[@"name"];
    NSString *guid = userInfoRequest[@"guid"];
    
    if ([type isEqualToString:kRequestTypeReuqest]) {
        
        if ([name isEqualToString:kRequestNameIsAlive]) {
            
            [[CHIAppClient sharedInstance] sendRequestType:kRequestTypeResponse
                                                      name:kRequestNameIsAlive
                                                    params:@{ @"senderId"        : guid,
                                                              @"bundleIdentifier": [NSBundle mainBundle].bundleIdentifier
                                                              }
                                                completion:nil
             ];
        } else if ([name isEqualToString:kRequestNameIsReady]) {
            
            [[CHIAppClient sharedInstance] sendRequestType:kRequestTypeStatus
                                                      name:kRequestNameIsReady
                                                    params:@{ @"senderId"        : guid,
                                                              @"bundleIdentifier": [NSBundle mainBundle].bundleIdentifier
                                                              }
                                                completion:nil
             ];
        }
    } else if ([type isEqualToString:kRequestTypeCommand]) {
        
//        NSLog(@"CLIENTNOTIFICATION COMMAND %@", notification);
        if ([name isEqualToString:kRequestNameGrab] && _enable) {
            
            [self analyzeWithParams:userInfoRequest[@"params"]];
//            NSLog(@"CLIENTNOTIFICATION COMMAND GRAB %@", notification);
        } else if ([name isEqualToString:kRequestNameDisableClient]) {
            
            _enable = NO;
        } else if ([name isEqualToString:kRequestNameEnableClient]) {
            
            _enable = YES;
            NSDictionary* environ = [[NSProcessInfo processInfo] environment];
            _inSandbox = ([environ objectForKey:@"APP_SANDBOX_CONTAINER_ID"] != nil);
        } else if ([name isEqualToString:kRequestNameStopRecording]) {
            
            [_analyzer stopRecording];
        } else if ([name isEqualToString:kRequestCreateProjectPath] && _enable) {
            
            _projectPath = nil;
            _projectPath = [userInfoRequest[@"params"][RequestParams.path] stringByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]];
            [CHIInjectedAppManager createDirectoryPath:_projectPath];
            [_analyzer resetWithStorePath:_projectPath];
            _fontsServerDirectoryPath = [[_projectPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:kFontsFolder];
            [CHIInjectedAppManager sendFontsToServer];
        }
    }
}

+ (void)analyzeWithParams:(NSDictionary *)params {
    
    NSString *objectName = params[@"objectName"];
    NSString *frameName  = params[@"time"];
    [_analyzer processWithFrameName:frameName andGrab:objectName];
}

+ (void)createDirectoryPath:(NSString *)path {
    
//    if (_inSandbox) {
    
        [[CHIAppClient sharedInstance] sendRequestType: kRequestTypeBridge
                                                  name: kRequestNameCreateDirectory
                                                params: @{ RequestParams.path: path }
                                            completion: nil];
//    } else {
//
//        if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
//            [[NSFileManager defaultManager] createDirectoryAtPath:path
//                                      withIntermediateDirectories:YES
//                                                       attributes:nil
//                                                            error:nil];
//        }
//    }
}

+ (void)writeObject:(id)object path:(NSString *)path {
    
//    if (_inSandbox) {
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            id objectToSend = object;
            NSString *dataType = @"any";
            if ([object isKindOfClass:[NSData class]]) {
                
                objectToSend = [object base64EncodedString];
                dataType = @"NSData";
            }
            [[CHIAppClient sharedInstance] sendRequestType: kRequestTypeBridge
                                                      name: kRequestNameWriteData
                                                    params: @{ RequestParams.path    : path,
                                                               RequestParams.data    : objectToSend,
                                                               RequestParams.dataType: dataType
                                                               }
                                                completion: nil];
        });
//    } else {
//
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//            if ([object isKindOfClass:[NSData class]] || [object isKindOfClass:[NSDictionary class]] || [object isKindOfClass:[NSArray class]]) {
//
//                [(NSData *)object writeToFile:path atomically:NO];
//            } else {
//
//                if ([object isKindOfClass:[NSString class]]) {
//
//                    [(NSString *)object writeToFile:path atomically:NO encoding:NSUTF8StringEncoding error:nil];
//                }
//            }
//        });
//    }
}

+ (void)copyItem:(NSString *)fromPath to:(NSString *)toPath {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[CHIAppClient sharedInstance] sendRequestType: kRequestTypeBridge
                                                  name: kRequestNameCopyItem
                                                params: @{ RequestParams.fromPath: fromPath,
                                                           RequestParams.toPath  : toPath
                                                           }
                                            completion: nil];
    });
}

+ (NSString *)serverFilePathForFontNameInBundle:(NSString *)fontName {
    
    __block NSString *fontFileName;
    __block NSUInteger index = NSNotFound;
    [_fontsLocalPathes enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        fontFileName = [obj lastPathComponent];
        if ([[fontFileName stringByDeletingPathExtension] isEqualToString:fontName]) {
            
            index = idx;
            *stop = YES;
        }
    }];
    
    NSString *result;
    
    if (index != NSNotFound) {
        
        NSString *projectRootFolder = [[_projectPath stringByDeletingLastPathComponent] lastPathComponent];
        NSRange projectRootFolderRange = [_fontsServerDirectoryPath rangeOfString:projectRootFolder];
        if (projectRootFolderRange.location != NSNotFound) {
            
            NSString *fontsServerRelativeDirectoryPath = [_fontsServerDirectoryPath substringFromIndex:projectRootFolderRange.location];
            result = [fontsServerRelativeDirectoryPath stringByAppendingPathComponent:fontFileName];
        }
    }
    
    return result;
}

#pragma mark - Private method

+ (void)sendFontsToServer {
    
    [CHIInjectedAppManager createDirectoryPath:_fontsServerDirectoryPath];
    
    for (NSURL *fontLocalURL in _fontsLocalPathes) {
        
        NSData *fontFileData = [NSData dataWithContentsOfURL:fontLocalURL];
        NSString *fontFileName = [fontLocalURL lastPathComponent];
        NSString *fontServerWritePath = [_fontsServerDirectoryPath stringByAppendingPathComponent:fontFileName];
        [CHIInjectedAppManager writeObject:fontFileData path:fontServerWritePath];
    }
}

@end
