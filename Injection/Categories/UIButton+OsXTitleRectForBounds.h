//
//  UIButton+OsXTitleRectForBounds.h
//  iOSInjection
//
//  Created by Sergey Voronov on 7/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (OsXTitleRectForBounds)

- (CGRect)titleRectForBounds:(CGRect)rect;

@end
