//
//  CHIAppClientServerRequest.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAppClientServerRequest.h"
#import "CHIUIUtilities.h"
#import "NSObject+ClassName.h"

@implementation CHIAppClientServerRequest

+ (instancetype)requestWithType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params {
    
    CHIAppClientServerRequest *newRequest = [[CHIAppClientServerRequest alloc] init];
    newRequest.type = type;
    newRequest.name = name;
    newRequest.params = params;
    return newRequest;
}

+ (instancetype)requestFromJSONString:(NSString *)JSONString {
    
    CHIAppClientServerRequest *newRequest = [[CHIAppClientServerRequest alloc] init];

    NSData *jsonData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *requestDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (error) {
        
        NSLog(@"ERROR: %@", error.localizedDescription);
    } else if (!requestDict.count) {
        
        NSLog(@"ERROR: Deserialized JSON data is empty.");
    } else {
        
        newRequest.type   = requestDict[@"type"];
        newRequest.name   = requestDict[@"name"];
        newRequest.params = requestDict[@"params"];
        newRequest.guid   = requestDict[@"guid"];
    }
    return newRequest;
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        self.guid = [CHIUIUtilities GUID];
        self.clients = [NSMutableArray array];
    }
    
    return self;
}

- (NSDictionary *)dictionary {
    
    return @{@"type":self.type, @"name":self.name, @"params":self.params, @"guid":self.guid, @"class":[self className]};
}

- (NSString *)JSONString {
    
    NSString *result;
    
    NSError *error;
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[self dictionary] options:0 error:&error];
    if (error) {
        
        NSLog(@"ERROR: %@", error.localizedDescription);
    } else if (data.length == 0) {
        
        NSLog(@"ERROR: Serialized dictionary data is empty.");
    } else {
        
        result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    
    return result;
}

- (NSString *)description {
    
    return [[self dictionary] description];
}

@end
