//
//  UIButton+OsXTitleRectForBounds.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIButton+OsXTitleRectForBounds.h"

@implementation UIButton (OsXTitleRectForBounds)

- (CGRect)titleRectForBounds:(CGRect)rect {
    CGRect result = CGRectZero;
    if (self.currentTitle.length) {
        result = self.titleLabel.frame;
    }
    return result;
}

@end
