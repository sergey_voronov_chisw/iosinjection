//
//  Constants.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef Constants_h
#define Constants_h

// Нет изменений
#define kChangeNone 0
// Перерендерить
#define kChangeVisibleState 1 << 0
// Изменился фрейм (только координата)
#define kChangeFrame 1 << 1
// Изменился текст
#define kChangeText 1 << 2
//// Изменились параметры текста - цвет, выравнивание и тд
//#define kChangeTextStyle 1 << 3
//// Изменился шрифт
//#define kChangeFont 1 << 4

//#define kDefaultAppIDToInject @"com.chisw.testLayer"

//#define kDefaultAppIDToInject @"com.viber.osx"
//#define kDefaultAppIDToInject @"com.skype.skype"
//#define kFinderID @"com.apple.finder"
//#define kDefaultAppIDToInject @"CHI.testAppForGrab"
//#define kDefaultAppIDToInject @"net.sourceforge.sqlitebrowser"
//#define kDefaultAppIDToInject @"com.operasoftware.Opera"


//#define kProtocolName @"comCHISWLR"

#define kClientNotification             @"kClientNotification"
#define kAOGrabFinishNotification       @"kAOGrabFinishNotification"
//#define kShowMessageNotification        @"kShowMessageNotification"
#define kGrameChangedNotification       @"frameChanged"
#define kGrabTargetAllNotification      @"grabTarget_All"
#define kNotificationUIViewDealloc      @"kNotificationUIViewDealloc"
//#define kNotificationResetAO            @"kNotificationResetAO"
#define kNotificationDeleteAO           @"kNotificationDeleteAO"

#define kNotificationParamFrameName     @"frameName"
#define kNotificationUserInfoKeyRequest @"request"

/*
#define kCameraChangePositionNotification @"kCameraChangePositionNotification"
#define kElementSelected @"kElementSelected"
#define kElementImageViewUpdateFrame @"kElementImageViewUpdateFrame"
#define kBranchChangeState @"kBranchChangeState"

#define kParamNameObject     @"Object 9999/2/1/2/1/2"
#define kParamNameUrl        @"Url 9999/2/1/2/1/420"
#define kParamNameMaskObject @"Object 9999/2/1/3/1/2"
#define kParamNameWidth      @"Width 9999/2/1/2/1/300"
#define kParamNameHeight     @"Height 9999/2/1/2/1/301"
#define kParamNameVisible    @"Visible 9999/2/1/2/1/4"
#define kParamNamePath       @"Path 9999/2/1/2/1/6"

#define ParamKeyWithName(__name__) [__name__ componentsSeparatedByString:@" "][1]
#define ParamNameWithName(__name__) [__name__ componentsSeparatedByString:@" "][0]

#define kParseModeRecording 1
#define kParseModeHTML      2

#define kCollapseTemplates @"collapse_templates"

#define kTitleColor [UIColor colorWithDeviceRed:0.42 green:0.56 blue:0.92 alpha:1]
*/
#define kPdfFileName   @"object.pdf"
#define kPngFileName   @"object.png"
#define kPlistFileName @"grabInfo.plist"

#define kAssociatedObjectName @"injected_attachment"
/*
#define kMouseEventNone           0
#define kMouseEventLeftClickDown  1
#define kMouseEventRightClickDown 2
#define kMouseEventOtherClickDown 3
#define kMouseEventLeftClickUp    4
#define kMouseEventRightClickUp   5
#define kMouseEventOtherClickUp   6

#define kKeyboardEventNone        0
#define kKeyboardEventKeyPress    1

#define kWorkedSuffix @"_work"

#define kFontScaleFactor 2
*/
#define kFontsFolder       @"Fonts"
/*#define kMouseFolder       @"Mouse"
#define kSoundsFolder      @"Sounds"
#define kVideoRecordFolder @"VideoRecord"
#define kHistoryFolder     @"History"*/


// Associated objects info

typedef const struct {
    __unsafe_unretained NSString *type;
    __unsafe_unretained NSString *class;
    __unsafe_unretained NSString *aoclass;
    __unsafe_unretained NSString *wasRendered;
    __unsafe_unretained NSString *zIndex;
    __unsafe_unretained NSString *visible;
    __unsafe_unretained NSString *margins;
    __unsafe_unretained NSString *frameName;
    __unsafe_unretained NSString *frame;
    __unsafe_unretained NSString *visibleFrame;
    __unsafe_unretained NSString *backgroundColor;
    __unsafe_unretained NSString *text;
    __unsafe_unretained NSString *textStyle;
    __unsafe_unretained NSString *placeholder;
    __unsafe_unretained NSString *fontName;
    __unsafe_unretained NSString *fontSize;
    __unsafe_unretained NSString *fontPath;
    __unsafe_unretained NSString *lastVisibleRenderFrameName;
    __unsafe_unretained NSString *lastChangesFrameName;
    __unsafe_unretained NSString *shouldParseText;
} AssociatedObjectInfo;
extern AssociatedObjectInfo const AOInfo;

typedef const struct {
    __unsafe_unretained NSString *text;
    __unsafe_unretained NSString *image;
    __unsafe_unretained NSString *apartText;
} AssociatedObjectInfoType;
extern AssociatedObjectInfoType const AOInfoType;


// Request params

typedef const struct {
    
    // Request type: Command
    __unsafe_unretained NSString *id;
    __unsafe_unretained NSString *frameToRec;
    __unsafe_unretained NSString *frame;
    __unsafe_unretained NSString *path;       // Bridge/WriteData as well
    __unsafe_unretained NSString *zIndex;
    
    // Request type: Bridge
    //   Write Data
    __unsafe_unretained NSString *data;
    __unsafe_unretained NSString *dataType;
    //   Copy Item
    __unsafe_unretained NSString *fromPath;
    __unsafe_unretained NSString *toPath;
    
    // Request type: Anylize Ignored
    __unsafe_unretained NSString *frameName;
    
    // Request type: Rogues Finish Grab
    __unsafe_unretained NSString *time;
} RequestParamsStruct;
extern RequestParamsStruct const RequestParams;

/*
// Editor

#define kTextEditingProcessFlagUnknown 0
#define kTextEditingProcessFlagUpdate  1
#define kTextEditingProcessFlagDisable 2

#define kMoveSelectionNone  0
#define kMoveSelectionLeft  1
#define kMoveSelectionRight 2
#define kMoveSelectionDown  3
#define kMoveSelectionUp    4

#define kCropModeUnknown 0
#define kCropModeDisable 1
#define kCropModeEnable  2

#define kRequestTypeUnknown      0
#define kRequestTypeContentLayer 1
#define kRequestTypeFrameName    2


// Undo Manager

#define kUndoRedoFiredNotification @"kUndoRedoFiredNotification"

#define kEventCreateDirectory @"createDirectory"
#define kEventRemoveDirectory @"removeDirectory"
#define kEventElementInfo     @"elementInfo"
#define kEventElementPath     @"elementPath"
#define kEventElementName     @"elementName"
#define kEventElementFullName @"elementFullName"
#define kEventCacheFile       @"cacheFile"
#define kEventMoveFile        @"moveFile"
#define kEventValueForKey     @"valueForKey"
#define kEventLayer           @"layer"
#define kEventDeleteChild     @"deleteChild"

// Interpolation

typedef NS_ENUM(NSInteger, InterpolationType) {
    InterpolationTypeMultiply = -1,
    InterpolationTypeNone = 0,
    InterpolationTypeLinear,
    InterpolationTypeEaseIn,
    InterpolationTypeEaseOut,
    InterpolationTypeEaseInOut
};

typedef NS_ENUM(NSInteger, InterpolationFactor) {
    InterpolationFactorDefault = 1
};

// Transformations

#define kSelectionModeNone      0
#define kSelectionModeMove      1
#define kSelectionModeScale     2
#define kSelectionModeCrop      3
#define kSelectionModeTranslate 4
#define kSelectionModeSize      5

typedef NS_ENUM(NSInteger, DragDirection) {
    DragDirectionNone,
    DragDirectionHorizontal,
    DragDirectionVertical
};

typedef NS_ENUM(NSInteger, ToolsMode) {
    ToolsModeDrag,
    ToolsModeRectangleSelection,
    ToolsModeSize,
    ToolsModeCrop
};
*/
#endif /* Constants_h */
