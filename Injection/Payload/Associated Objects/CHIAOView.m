//
//  CHIAOView.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 09.08.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAOView.h"
#import "CHIUIUtilities.h"
#import "UIBezierPath+CHIAddition.h"
#import "UIColor+CHIColorStore.h"
#import "CHIAppClient.h"
#import "CHIInjectedAppManager.h"
#import "NSObject+ClassName.h"
#import "UIView+VisibleRect.h"
#import "NSObject+ConvertPoints.h"
#import "NSObject+Selectors.h"
#import "CALayer+PDF.h"
#import "CGPathHelper.h"
#import "Constants.h"

#import <QuartzCore/QuartzCore.h>
#import <objc/message.h>
#import <ImageIO/ImageIO.h> // for CGImage...
#import <MobileCoreServices/MobileCoreServices.h> // for kUTTypePNG

@interface CHIAOView ()

@property (nonatomic) BOOL _clear;
@property (nonatomic, weak) id _lastImage;

@end

@implementation CHIAOView

- (instancetype)initWithTargetObject:(UIView *)object path:(NSString *)path {
    
    self = [super initWithTargetObject:object path:path];
    
    if (self) {
        self._clear = [self.target isMemberOfClass:[UIView class]]
                        &&
                      (CGColorEqualToColor(self.target.backgroundColor.CGColor, [UIColor clearColor].CGColor) ||
                       self.target.alpha == 0.);
    }
    
    return self;
}

- (void)resetWithPath:(NSString *)path {
    
//    if ([self.target.className isEqualToString:@"ChatInputResizableContainerView"]) {
//        NSLog(@"");
//    }
    
    [super resetWithPath:path];
    
    self._lastImage = nil;
}

- (void)setupObserver {
    
    [super setupObserver];
    
    if ([NSStringFromClass(self.target.class) isEqualToString:@"NSTitlebarContainerView"]) {
        [self addObserverForKeyPath:@"window.isKeyWindow"];
    }
    [self addObserverForKeyPath:@"layer.backgroundColor"];
    if ([self.target respondsToSelector:@selector(image)]) {
        
        self._lastImage = [(id)self.target image];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    
    if ([keyPath isEqualToString:@"layer.backgroundColor"] ||
        [keyPath isEqualToString:@"window.isKeyWindow"   ]) {
        
        self.wasChanges |= kChangeVisibleState;
    }
}

- (BOOL)shouldGrab {
    
//    if ([[self.target className] isEqualToString:@"LOTAnimationView"]) {
//        NSLog(@"%s: Animation keys: %@", __FUNCTION__, [[self.target layer] animationKeys]);
////    UIView *target = self.target;
////    NSArray *animationKeys = [target.layer animationKeys];
////    if (animationKeys.count > 0) {
////        self.wasChanges |= kChangeVisibleState;
////    }
//    }
    if ([self.target respondsToSelector:@selector(image)] &&
        self._lastImage != [(id)self.target image]) {
        
        self.wasChanges |= kChangeVisibleState;
        self._lastImage = [(id)self.target image];
    }
    return [super shouldGrab];
}

- (void)saveGradientLayer:(CALayer *)layer withInfo:(NSMutableDictionary *)info {
    
//    if ([[self.target className] isEqualToString:@"IGStoryGradientRingView"]) {
//        NSLog(@"===== %s: %@\nInfo: %@", __FUNCTION__, NSStringFromCGRect(self.target.frame), info);
//    }
    
    CAGradientLayer *gradient = (id)layer;
//    if ([[self.target className] isEqualToString:@"IGStoryGradientRingView"]) {
//        NSLog(@"===== %s: %@\n: Corner radius: %f\nMask: %@", __FUNCTION__, NSStringFromCGRect(self.target.frame), gradient.cornerRadius, gradient.mask);
//    }
    NSArray *colors = gradient.colors;
    NSMutableArray *_colors = [NSMutableArray array];
    for (id colorRefId in colors) {
        CGColorRef colorRef = (__bridge CGColorRef)(colorRefId);
        [_colors addObject:[UIColor stringRepresentationFromCGColor:colorRef]];
    }
    if (_colors.count == 0) {
        
//        NSString *fmt = @" (%@)";
//        NSLog(@"===== %s: No color%@", __FUNCTION__, [[self.target className] isEqualToString:@"IGStoryGradientRingView"] ? [NSString stringWithFormat:fmt, NSStringFromCGRect(self.target.frame)] : @"");
    } else {
//        NSString *frameString, *startPointString, *endPointString;
//        if (CGRectEqualToRect(layer.bounds, CGRectMake(0, 0, 40, 40))) {
//            frameString      = NSStringFromCGRect(CGRectMake(0, layer.bounds.size.height, 40, 0));
//            startPointString = NSStringFromCGPoint(CGPointMake(0, layer.bounds.size.height - gradient.startPoint.y));
//            endPointString   = NSStringFromCGPoint(CGPointMake(1, layer.bounds.size.height - gradient.endPoint.y  ));
//        } else {
//            frameString      = NSStringFromCGRect(layer.bounds);
//            startPointString = NSStringFromCGPoint(gradient.startPoint);
//            endPointString   = NSStringFromCGPoint(gradient.endPoint  );
//        }
        NSDictionary *gradientDict = @{ @"colors"    : _colors,
                                        @"locations" : gradient.locations ? gradient.locations : @"",
                                        @"startPoint": NSStringFromCGPoint(/*[self.target osXPoint:*/gradient.startPoint/*]*/),
                                        @"endPoint"  : NSStringFromCGPoint(/*[self.target osXPoint:*/gradient.endPoint/*]*/),
                                        @"frame"     : NSStringFromCGRect(layer.bounds),
                                        @"zIndex"    : [NSString stringWithFormat:@"%zd", [[self.target className] hasPrefix:@""] ? -1 : [layer.superlayer.sublayers indexOfObject:layer]]
                                       };
        [CHIInjectedAppManager writeObject:gradientDict
                                      path:[self.writePath stringByAppendingPathComponent:[NSString stringWithFormat:@"gr_%p.plist", layer]]];
    }
}

- (void)saveMaskLayer:(CALayer *)layer withInfo:(NSMutableDictionary *)info {
    
    if ([layer isMemberOfClass:[CAShapeLayer class]]) {
//        UIBezierPath *path = [UIBezierPath bezierPathWithCGPath:[(CAShapeLayer *)layer path]];
        CGPathHelper *pathHelper = [[CGPathHelper alloc] initWithPathRef:[(CAShapeLayer *)layer path]];
        NSArray *pathArray = [pathHelper pathArray];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pathArray/*path*/];
        [CHIInjectedAppManager writeObject:data path:[self.writePath stringByAppendingPathComponent:@"mask.data"]];
    }
}

- (void)saveImageContentOfLayer:(CALayer *)layer withInfo:(NSMutableDictionary *)info index:(int)contentsIndex {
    
    UIImage *image = nil;
    if ([layer.delegate isKindOfClass:[UIImageView class]]) {
        image = (UIImage *)[(id)layer.delegate image];
    } else if ([layer.contents isKindOfClass:[UIImage class]]) {
        image = layer.contents;
    }
    
    NSString *contentWritePath = [self.writePath stringByAppendingPathComponent:[NSString stringWithFormat:@"content%d_%@", contentsIndex, self.targetName]];
    if (image) {
//        NSArray *reps = image.representations;
//        if (reps.count > 0) {
            [CHIInjectedAppManager createDirectoryPath:contentWritePath];
//            NSBitmapImageRep *rep = [reps firstObject];
            NSData *data = UIImagePNGRepresentation(image); // nil;
//            if ([rep isKindOfClass:[NSBitmapImageRep class]]) {
//                id ex = nil;
//                @try {
//                    data = [rep representationUsingType:NSPNGFileType properties:@{}];
//                } @catch (NSException *exception) {
//                    ex = exception;
//                } @finally {
                    [self writeData:data andInfo:info ofLayer:layer useBounds:NO atPath:contentWritePath];
//                    return;
//                }
//            }
//        }
    } else if ([[layer.contents className] isEqualToString:@"_UIImageLayerContents"]) {
        
        [CHIInjectedAppManager createDirectoryPath:contentWritePath];
        id contents = layer.contents;
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGImageRef cgImage = NULL;
        
//        SEL selector = NSSelectorFromString(@"_CGImageWithColorSpace:");
//        IMP pointerToCGImageWithColorSpaceMethod = [contents methodForSelector:selector];
//        pointerToCGImageWithColorSpaceMethod(contents, selector, colorSpace);
        
        id value = //objc_msgSend(contents, selector, colorSpace);
//            [contents performSelector:selector withObject:(__bridge id)colorSpace];
            [NSObject performSelectorWithName:@"_CGImageWithColorSpace:" andParams:@[(__bridge id)colorSpace] ofObject:contents];
        
        cgImage = (__bridge CGImageRef)(value);
        CFMutableDataRef newImageData = CFDataCreateMutable(NULL, 0);
        CGImageDestinationRef destination = CGImageDestinationCreateWithData(newImageData, kUTTypePNG, 1, NULL);
        CGImageDestinationAddImage(destination, cgImage, nil);
        CGImageDestinationFinalize(destination);
        NSData *data = (__bridge  NSData *)newImageData;
        [self writeData:data andInfo:info ofLayer:layer useBounds:YES atPath:contentWritePath];

        CFRelease(destination);
        CGColorSpaceRelease(colorSpace);
    }
}

- (void)writeData:(NSData *)data
          andInfo:(NSDictionary *)info
          ofLayer:(CALayer *)layer
        useBounds:(BOOL)useBounds
           atPath:(NSString *)path {
    
    [CHIInjectedAppManager writeObject:data path:[path stringByAppendingPathComponent:kPngFileName]];
    
    NSMutableDictionary *contentGrabInfo = [info mutableCopy];
    contentGrabInfo[AOInfo.type  ] = AOInfoType.image;
    contentGrabInfo[AOInfo.frame ] = NSStringFromCGRect(useBounds ? layer.bounds : layer.frame);
    contentGrabInfo[AOInfo.zIndex] = @([layer.superlayer.sublayers indexOfObject:layer]);
    if ([[self.target className] isEqualToString:@"IGDeclarativeScrollCollectionView"] ||
        [[self.target className] isEqualToString:@"IGMainAppHeader"]) {
        NSLog(@"===== %s: %@ (%@), zIndex: %zd", __FUNCTION__, [self.target class], [self class], info[AOInfo.zIndex]);
    }
    [CHIInjectedAppManager writeObject:contentGrabInfo path:[path stringByAppendingPathComponent:kPlistFileName]];
}

- (void)drawLayer:(CALayer *)layer withInfo:(NSMutableDictionary *)info {
    
    if ([NSStringFromClass(self.target.class) isEqualToString:@"NSTitlebarContainerView"]) {
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.target.bounds;
        NSArray *colors;
        if ([self.target.window isKeyWindow]) {
            
            colors = @[(id)[[UIColor colorWithRed:201./255 green:201./255 blue:201./255 alpha:0.3] CGColor],
                       (id)[[UIColor colorWithRed:244./255 green:244./255 blue:244./255 alpha:1] CGColor]];
        } else {
            
            colors = @[(id)[[UIColor colorWithRed:244./255 green:244./255 blue:244./255 alpha:1] CGColor],
                       (id)[[UIColor colorWithRed:244./255 green:244./255 blue:244./255 alpha:1] CGColor]];
        }
        gradient.colors = colors;
        [self saveGradientLayer:gradient withInfo:info];
    }
    BOOL render = NO;
    NSMutableArray *sublayers = [NSMutableArray array];
    int contentsIndex = 0;
//    UIView *target = self.target;
    
    for (CALayer *sublayer in layer.sublayers) {
        
        if ((sublayer.delegate == (id)self.target) ||
            (sublayer.delegate == nil && sublayer.name == nil) ||
            [sublayer.name isEqualToString:@"CUIWindowFrameLayer"]) {
            
            if ([[sublayer className] isEqualToString:@"CAGradientLayer"]) {
                
                [self saveGradientLayer:sublayer withInfo:info];
            } else if ([sublayer.contents isKindOfClass:[UIImage class]] ||
                       [sublayer.delegate isKindOfClass:[UIImageView class]]) {
                
                [self saveImageContentOfLayer:sublayer withInfo:info index:contentsIndex];
                contentsIndex++;
            } else {
                
                [sublayers addObject:sublayer];
                render = YES;
            }
        } else if ([[sublayer className] isEqualToString:@"SKGLVideoRendererLayer"]) {
            
            self.wasChanges |= kChangeVisibleState;
            CGRect visibleFrame = [self.target visibleRect];
            CGRect frameRelativeToWindow = [self.target convertRect:visibleFrame toView:nil];
            CGRect frameRelativeToScreen = frameRelativeToWindow; // [self.target.window convertRectToScreen:frameRelativeToWindow];
            CGRect frame = CGRectMake(self.target.frame.origin.x + visibleFrame.origin.x,
                                      self.target.frame.origin.y + visibleFrame.origin.y,
                                      visibleFrame.size.width,
                                      visibleFrame.size.height);
            NSDictionary *params = @{ RequestParams.id        : [NSString stringWithFormat:@"%p", self.target],
                                      RequestParams.frameToRec: NSStringFromCGRect([self.target osXRect:frameRelativeToScreen]),
                                      RequestParams.frame     : NSStringFromCGRect([self.target osXRect:frame]),
                                      RequestParams.path      : self.writePath,
                                      RequestParams.zIndex    : @(self.zIndex)
                                      };
            [[CHIAppClient sharedInstance] sendRequestType:kRequestTypeCommand
                                                      name:kRequestNameProcessVideo
                                                    params:params
                                                completion:nil];
            self.wasRendered = YES;
            if (!self.sendNotificationToServerOnDealloc) {
                
                self.sendNotificationToServerOnDealloc = @{ @"name"  : kRequestNameStopVideo,
                                                            @"params": @{ RequestParams.id: [NSString stringWithFormat:@"%p", self.target] }
                                                            };
            }
        }
    }
    
    if ([[layer.contents className] isEqualToString:@"_UIImageLayerContents"]) {
        
        [self saveImageContentOfLayer:layer withInfo:info index:contentsIndex];
    }
    
    CALayer *maskLayer = layer.mask;
    if (maskLayer) {
        
        [self saveMaskLayer:maskLayer withInfo:info];
    }
    if (!render && (layer.backgroundColor || layer.contents)) {
        
        render = YES;
    }
    if ([[layer className] isEqualToString:@"CAGradientLayer"]) {
        
        [self saveGradientLayer:layer withInfo:info];
    } else if (render) {
        
        NSData *pdfData = [layer pdfDataWithDataConsumerWithSublayersToHide:sublayers];
        if (pdfData) {
            
            [CHIInjectedAppManager writeObject:pdfData
                                          path:[self.writePath stringByAppendingPathComponent:kPdfFileName]];
        }
        
        self.wasRendered = YES;
    }
}

- (void)grabVisibleWithInfo:(NSMutableDictionary *)info {
    
#warning Temporary hack for LM-81
    if ([self.target.className isEqualToString:@"IGStoryGradientRingView"]) {
        info[AOInfo.visible] = @"0"; // Hide gradient view (that should be a periphery of circle instead of solid circle)
        return;
    }
        
    if (!self._clear) {
        
        info[AOInfo.aoclass] = NSStringFromClass([self class]);
        UIView *target = self.target;
        info[AOInfo.wasRendered] = @(YES);
        [self drawLayer:target.layer withInfo:info];
        [self commonVisibleGrabWithInfo:info];
    }
}

@end
