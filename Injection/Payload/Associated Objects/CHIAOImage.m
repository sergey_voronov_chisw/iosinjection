//
//  CHIAOImage.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 09.08.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAOImage.h"
#import "CHIInjectedAppManager.h"
#import "CALayer+PDF.h"
#import "Constants.h"

@interface CHIAOImage ()

@property (nonatomic) NSData *data;

@end

@implementation CHIAOImage

- (BOOL)shouldGrab {
    NSData *imageData = [self.target.layer pngData];
    if (imageData && ([imageData isEqualToData:self.data] == NO)) {
        self.data = imageData;
        self.wasChanges |= kChangeVisibleState;
        return YES;
    }
    return NO;
}

- (void)grabVisibleWithInfo:(NSMutableDictionary *)info {
    self.wasRendered = YES;
    [CHIInjectedAppManager writeObject:self.data path:[self.writePath stringByAppendingPathComponent:kPngFileName]];
    [self commonVisibleGrabWithInfo:info];
    info[AOInfo.type] = AOInfoType.image;
}

@end
