//
//  CHIAOVideo.m
//  LayeredScreenRecorder
//
//  Created by Yuri Gavrilenko on 29.08.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAOVideo.h"
#import "Constants.h"

@implementation CHIAOVideo

- (instancetype)initWithTargetObject:(UIView *)object path:(NSString *)path {
    self = [super initWithTargetObject:object path:path];

    if (self) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"forceFrameChanged" object:nil];
    }

    return self;
}

//- (void)grabVisibleWithInfo:(NSMutableDictionary *)info {
//    CGWindowID windowID = (CGWindowID)[self.target.window windowNumber];
//    CGRect frameRelativeToWindow = [self.target convertRect:[self.target bounds] toView:nil];
//    CGRect frameRelativeToScreen = [self.target.window convertRectToScreen:frameRelativeToWindow];
//    CGImageRef image = CGWindowListCreateImage(frameRelativeToScreen, kCGWindowListOptionIncludingWindow, windowID, kCGWindowImageDefault);
//    [self writeCGImage:image toFile:[self.writePath stringByAppendingPathComponent:@"img.png"]];
//}
//
//- (BOOL)shouldGrab {
//    self.wasChanges = self.wasChanges | kChangeVisibleState;
//    return YES;
//}
//
//- (BOOL) writeCGImage:(CGImageRef) image toFile:(NSString *) path {
//    CFURLRef url = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
//    CGImageDestinationRef destination = CGImageDestinationCreateWithURL(url, kUTTypePNG, 1, NULL);
//    if (!destination) {
//        NSLog(@"Failed to create CGImageDestination for %@", path);
//        return NO;
//    }
//    
//    CGImageDestinationAddImage(destination, image, nil);
//    
//    if (!CGImageDestinationFinalize(destination)) {
//        NSLog(@"Failed to write image to %@", path);
//        CFRelease(destination);
//        return NO;
//    }
//    
//    CFRelease(destination);
//    return YES;
//}

@end
