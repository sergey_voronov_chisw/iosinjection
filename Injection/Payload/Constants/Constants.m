//
//  Constants.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/17/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "Constants.h"

AssociatedObjectInfo const AOInfo = {
    .type                       = @"type",
    .class                      = @"class",
    .aoclass                    = @"aoclass",
    .wasRendered                = @"wasRendered",
    .zIndex                     = @"zIndex",
    .visible                    = @"visible",
    .margins                    = @"margins",
    .frameName                  = @"frameName",
    .frame                      = @"frame",
    .visibleFrame               = @"visibleFrame",
    .backgroundColor            = @"backgroundColor",
    .text                       = @"text",
    .textStyle                  = @"textStyle",
    .placeholder                = @"placeholder",
    .fontName                   = @"fontName",
    .fontSize                   = @"fontSize",
    .fontPath                   = @"fontPath",
    .lastVisibleRenderFrameName = @"lastVisibleRenderFrameName",
    .lastChangesFrameName       = @"lastChangesFrameName",
    .shouldParseText            = @"shouldParseText",
};

AssociatedObjectInfoType const AOInfoType = {
    .text      = @"text",
    .image     = @"image",
    .apartText = @"apartText"
};

RequestParamsStruct const RequestParams = {
    
    // Request type: Command
    .id         = @"id",
    .frameToRec = @"frameToRec",
    .frame      = @"frame",
    .path       = @"path",      // Bridge/WriteData as well
    .zIndex     = @"zIndex",
    
    // Request type: Bridge
    //   Write Data
    .data       = @"data",
    .dataType   = @"dataType",
    //   Copy Item
    .fromPath   = @"fromPath",
    .toPath     = @"toPath",
    
    // Request type: Anylize Ignored
    .frameName  = @"frameName",
    
    // Request type: Rogues Finish Grab
    .time       = @"time"
};
