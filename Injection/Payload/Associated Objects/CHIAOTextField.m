//
//  CHIAOTextField.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 09.08.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAOTextField.h"
#import "Constants.h"
#import "CHIUIUtilities.h"

#import "NSObject+ClassName.h"
#import "NSObject+Selectors.h"
#import "UIView+Hierarchy.h"

@interface CHIAOTextField ()

@property (nonatomic) NSUInteger _textHash;

@end

@implementation CHIAOTextField

//- (instancetype)initWithTargetObject:(UIView *)object path:(NSString *)path {
//    
//    self = [super initWithTargetObject:object path:path];
//    
//    if (self) {
//    }
//    return self;
//}
//
// Заглушка для вызова этого метода у target
- (UIEdgeInsets)bubbleMargins {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)resetWithPath:(NSString *)path {
    
    [super resetWithPath:path];
    
    self._textHash = [self currentHash];
    if ([self.target isKindOfClass:[UITextField class]]) {
        
        // Этот костыль нужен по причине непонятного обрезания текстфилда слева, только в случае когда он лежит в скайпе на HistoryLastMessageCellView
        double deltaX = 0;
        if (/*![(UITextField *)(self.target) isBezeled] && */[self.target.superview isKindOfClass:[UITableViewCell class]] && self.target.frame.origin.x < 0) {
            deltaX = 2;
        }
        /*NSCell*/UITextField *cell = /*[*/(UITextField *)self.target /*cell]*/;
        CGRect rect = [cell /*titleRectForBounds*/textRectForBounds:self.target.bounds];
        self.margins = CGRectMake(rect.origin.x + deltaX, rect.origin.y, self.target.frame.size.width - rect.size.width - rect.origin.x, self.target.frame.size.height - rect.size.height - rect.origin.y);
        if ([self.target respondsToSelector:@selector(bubbleMargins)]) {
            
            UIEdgeInsets insets = [(id)self.target bubbleMargins];
            self.margins = CGRectMake(insets.left, insets.top, insets.right, insets.bottom);
        }
    }
}

- (void)setupObserver {
    
    [super setupObserver];
    
    [self addObserverForKeyPath:@"font"];
    [self addObserverForKeyPath:@"textColor"];
    [self addObserverForKeyPath:@"backgroundColor"];
    
    if ([self.target isKindOfClass:[UITextField class]]) {
        
        [self addObserverForKeyPath:@"alignment"];
        [self addObserverForKeyPath:@"placeholderString"];
        [self addObserverForKeyPath:@"lineBreakMode"];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    
    if ([keyPath isEqualToString:@"textColor"] ||
        [keyPath isEqualToString:@"alignment"] ||
        [keyPath isEqualToString:@"placeholderString"] ||
        [keyPath isEqualToString:@"lineBreakMode"]/* ||
        [keyPath isEqualToString:@"text"]*/) {
            
        self.wasChanges |= kChangeText;
    } else if ([keyPath isEqualToString:@"backgroundColor"]) {
        
        self.wasChanges |= kChangeVisibleState;
    }
}

- (void)grabTextWithInfo:(NSMutableDictionary *)info {
    
    info[AOInfo.type] = AOInfoType.text;
    
    if ([self.target isKindOfClass:[UITextField class]]) {
        
        info[AOInfo.placeholder] = [(id)self.target placeholder];
        info[AOInfo.text] = [(id)self.target text];
    } else if ([self.target isKindOfClasses:@[[UITextView class], [UILabel class]]]) {
        
//        info[AOInfo.placeholder] = [(id)self.target placeholder];
        info[AOInfo.text] = [(id)self.target text];
    } else if ([[self.target className] isEqualToString:@"RCTText"]) { // iOS Skype
        
        NSTextStorage *textStorage = [self.target performSelectorWithName:@"textStorage" andParams:nil];
        info[AOInfo.text] = [textStorage string];
    } else if ([[self.target className] isEqualToString:@"RCTTextField"]) { // iOS Skype
        
        id textField = [self.target performSelectorWithName:@"textField" andParams:nil];
        if (textField) {
            
            info[AOInfo.placeholder] = [textField placeholder];
            info[AOInfo.text] = [textField text];
        }
    }
}

- (void)grabTextStyleWithInfo:(NSMutableDictionary *)info {
    
    NSAttributedString *attributedString;
    if ([[self.target className] isEqualToString:@"RCTText"]) { // iOS Skype
        
        NSTextStorage *textStorage = [self.target performSelectorWithName:@"textStorage" andParams:nil];
        attributedString = textStorage;
    } else {
        
        attributedString = [self.target performSelector:@selector(attributedText)];
        if ([self.target isKindOfClass:[UITextField class]] && [[attributedString string] isEqualToString:@""]) {
            
            attributedString = ((UITextField *)self.target).attributedPlaceholder;
        }
    }
    
    if ([self.target respondsToSelector:@selector(font)]) {
        
        UIFont *font = [(id)self.target font];
        if (font) {
            
            info[AOInfo.fontName] = [font fontName];
            info[AOInfo.fontSize] = @([[[font fontDescriptor] objectForKey:UIFontDescriptorSizeAttribute] doubleValue]);
        }
    }
    info[AOInfo.textStyle] = [self textStyleForAttributedString:attributedString];
//    if ([[self.target className] isEqualToString:@"RCTText"]) {
//        [CHIUIUtilities textStyleForAttributedString:attributedString];
//    }
    info[AOInfo.margins] = @[@(self.margins.origin.x),
                             @(self.margins.origin.y),
                             @(self.margins.size.width),
                             @(self.margins.size.height)];
    
    CGColorRef cgBackgroundColor = [[/*(id)*/self.target backgroundColor] CGColor];
    if (cgBackgroundColor) {
        
        size_t count = CGColorGetNumberOfComponents(cgBackgroundColor);
        const CGFloat *components = CGColorGetComponents(cgBackgroundColor);
        
        NSMutableString *string = [NSMutableString string];
        [string appendFormat:@"%f", components[0]];
        for (int i = 1; i < count; i++) {
            
            [string appendFormat:@" %f", components[i]];
        }
        
        info[AOInfo.backgroundColor] = string;
    } else {
        
        info[AOInfo.backgroundColor] = @"0.0 0.0 0.0 0.0";
    }
    
}

- (NSUInteger)currentHash {
    
    if ([self.target isKindOfClass:[UITextField class]]) {
        
        return [[(id)self.target text] hash];
    }
    if ([[self.target className] isEqualToString:@"RCTTextField"]) { // iOS Skype
        
        id textField = [self.target performSelectorWithName:@"textField" andParams:nil];
        return [[textField text] hash];
    }
    
    return 0; // [[(id)self.target string] hash];
}

- (BOOL)shouldGrab {
    
    NSUInteger textHash = [self currentHash];
    
    if (textHash != self._textHash) {
        
        self.wasChanges |= kChangeText;
        self._textHash = textHash;
    }
    
    return [super shouldGrab];
}

@end
