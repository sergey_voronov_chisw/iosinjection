//
//  NSObject+ConvertPoints.h
//  iOSInjection
//
//  Created by Sergey Voronov on 8/4/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>

@interface NSObject (ConvertPoints)

- (CGPoint)osXPoint:(CGPoint)point;
- (CGRect)osXRect:(CGRect)rect;

@end
