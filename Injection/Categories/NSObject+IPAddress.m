//
//  NSObject+IPAddress.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "NSObject+IPAddress.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

@implementation NSObject (IPAddress)

+ (NSString *)ipAddress {
    NSString *address;
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    
    // retrieve the current interfaces - returns 0 on success
    int success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if (temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    break;
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
    
}

@end
