//
//  UIViewController+TopmostViewController.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/11/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIViewController+TopmostViewController.h"

@implementation UIViewController (TopmostViewController)

+ (UIViewController*)topmostViewControllerWithRootViewController:(UIViewController*)rootViewController {
    
    if (!rootViewController) {
        
        rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    }
    
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topmostViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topmostViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topmostViewControllerWithRootViewController:presentedViewController];
    } else {
        
        return rootViewController;
    }
}

@end
