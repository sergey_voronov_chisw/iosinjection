//
//  CHIAssociatedObject.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 25.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GrabType) {
    GrabTypeDefaultControl,
    GrabTypeBackground,
    GrabTypeImage,
    GrabTypeParameters,
};

@interface CHIAssociatedObject : NSObject

@property (nonatomic, assign) UIView *target;
@property (nonatomic, strong) NSString *projectRootPath;
@property (nonatomic, strong) NSString *targetName;
@property (nonatomic, strong) NSString *className;
@property (nonatomic, strong) NSString *superViewPath;
@property (nonatomic, strong) NSString *writePath;
@property (nonatomic, strong) NSString *frameName;
@property (nonatomic) NSInteger wasChanges;
@property (nonatomic, strong) NSString *lastVisibleRenderFrameName;
@property (nonatomic, strong) NSString *lastVisibleRenderObjectPath;
@property (nonatomic, strong) NSString *lastChangesFrameName;
@property (nonatomic) BOOL wasRendered;
@property (nonatomic) NSInteger zIndex;
@property (nonatomic) CGRect margins;
@property (nonatomic) CGRect lastScreenFrame;
@property (nonatomic) CGRect lastFrame;
@property (nonatomic) CGRect lastVisibleRect;
@property (nonatomic) BOOL isOnScreen;
@property (nonatomic, strong) NSDictionary *sendNotificationToServerOnDealloc;
@property (nonatomic, strong) NSDate *createDate;

- (instancetype)initWithTargetObject:(UIView *)object path:(NSString *)path;
- (void)setupObserver;
- (BOOL)shouldGrab;
- (void)grabTarget;
- (void)removeObserver;
- (void)addObserverForKeyPath:(NSString *)keyPath;
- (void)commonVisibleGrabWithInfo:(NSMutableDictionary *)info;
- (CGRect)convertedFrame;
- (NSDictionary *)textStyleForAttributedString:(NSAttributedString *) attributedString;
- (void)resetWithPath:(NSString *)path;
//- (void)mouseEntered:(UIEvent *)theEvent;
//- (void)mouseMoved:(UIEvent *)theEvent;
//- (void)mouseExited:(UIEvent *)theEvent;
- (UIView *)targetSuperview;

@end
