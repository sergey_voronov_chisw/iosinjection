//
//  CHIUIUtilities.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 20.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIUIUtilities.h"
#import <objc/runtime.h>
#import <CoreText/CoreText.h>
#import "NSObject+ClassName.h"
#import "NSObject+ConvertPoints.h"

@implementation CHIUIUtilities

+ (NSDictionary *)objectMethods:(id)object {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    unsigned count;
    Method *methods = class_copyMethodList([object class], &count);
    for (unsigned i = 0; i < count; i++) {
        
        Method method = methods[i];
        SEL sel = method_getName(method);
        if (sel) {
            
            NSString *name = NSStringFromSelector(sel);
            const char *typeEncoding = method_getTypeEncoding(method);
            NSString *typeEncodingString;
            if (typeEncoding) {
                
                typeEncodingString = [NSString stringWithCString:typeEncoding encoding:[NSString defaultCStringEncoding]];
            }
            result[name] = typeEncodingString ?: @"???";
        }
    }
    free(methods);
    
    return result;
}

+ (NSArray *)objectMethodsNames:(id)object {
    
    NSMutableArray *result = [NSMutableArray array];
    
    unsigned count;
    Method *methods = class_copyMethodList([object class], &count);
    for (unsigned i = 0; i < count; i++) {
        
        Method method = methods[i];
        SEL sel = method_getName(method);
        if (sel) {
            
            NSString *name = NSStringFromSelector(sel);
            [result addObject:name];
        }
    }
    free(methods);
    
    return result;
}

+ (NSDictionary *)objectProperties:(id)object {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    unsigned int count;
    objc_property_t *properties = class_copyPropertyList([object class], &count);
    for (unsigned i = 0; i < count; i++) {
        
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        if (propName) {
            
            const char *propType = [self getPropertyType:property];
            NSString *propertyName = [NSString stringWithCString:propName encoding:[NSString defaultCStringEncoding]];
            NSString *propertyType = [NSString stringWithCString:propType encoding:[NSString defaultCStringEncoding]];
            if (propertyType && propertyName) {
                
                result[[NSString stringWithFormat:@"%@ (%@)", propertyName, propertyType]] = @"";
            }
        }
    }
    free(properties);
    
    return result;
}

+ (NSArray *)objectPropertiesNames:(id)object {
    
    NSMutableArray *result = [NSMutableArray array];
    
    unsigned int count;
    objc_property_t *properties = class_copyPropertyList([object class], &count);
    for (unsigned i = 0; i < count; i++) {
        
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        if (propName) {
            
            NSString *propertyName = [NSString stringWithCString:propName encoding:[NSString defaultCStringEncoding]];
            if (propertyName) {
                
                [result addObject:propertyName];
            }
        }
    }
    free(properties);
    
    return result;
}

+ (NSArray *)layersForObject:(id)object {
    
    NSMutableArray *result = [NSMutableArray array];
    for (CALayer *layer in [object layer].sublayers) {
        
        [result addObject:@{@"class":[layer className]}];
    }
    return result;
}

+ (NSMutableDictionary *)grabObject:(id)object {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    result[@"superClass"] = [self superclassFromClassPath:nil ofClass:[object class]];
    result[@"objectName"] = [NSString stringWithFormat:@"%p", object];
    
    if ([object respondsToSelector:@selector(frame)]) {
        
        CGRect parentFrame = CGRectZero;
        if ([object respondsToSelector:@selector(superview)]) {
            
            parentFrame = [[object superview] frame];
        }
        result[@"frame"] = NSStringFromCGRect([object osXRect:[object frame]]);
    }
    
    if ([object isKindOfClass:[UIButton class]]) {
        
        [self grabButton:object params:result];
    } else if ([object isKindOfClass:[UITextField class]]) {
        
        [self grabTextField:object params:result];
    } else if ([object isKindOfClass:[UITextView class]]) {
        
        [self grabTextView:object params:result];
    } else if ([object isKindOfClass:[UIImageView class]]) {
        
        [self grabImageView:object params:result];
    }
    
    return result;
}

+ (NSString *)superclassFromClassPath:(NSString *)classPath ofClass:(Class)class {
    
    NSString *superclassName = NSStringFromClass([class superclass]);
    if (superclassName) {
        
        NSString *newPath = [NSString stringWithFormat:@"%@%@",
                                classPath ? [NSString stringWithFormat:@"%@ -> ", classPath] : @"",
                                superclassName
                             ];
        return [self superclassFromClassPath:newPath ofClass:[class superclass]];
    } else {
        
        return classPath;
    }
}

+ (UIFont *)fontFromTextStyleDictionary:(NSDictionary *)textStyle {
    
    NSString *fontPath = textStyle[@"fontPath"];
    NSString *fontName = textStyle[@"fontName"];
    NSNumber *fontSize = textStyle[@"fontSize"];
    
    UIFont *font = nil;
    
    if (fontPath) {
        
        font = [self loadFontWithFontName:fontName size:fontSize.floatValue path:fontPath];
    } else {
        
        font = [UIFont fontWithName:fontName size:fontSize.floatValue];
    }
    
    font = font == nil ? [UIFont systemFontOfSize:fontSize.floatValue] : font;
    
    return font;
}

+ (NSDictionary *)textStyleForAttributedString:(NSAttributedString *)attributedString {
    
    NSMutableDictionary *rangeDictionary = [NSMutableDictionary dictionary];
    [attributedString enumerateAttributesInRange:NSMakeRange(0, attributedString.length)
                                         options:0
                                      usingBlock:^(NSDictionary<NSString *,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
                                          
        rangeDictionary[NSStringFromRange(range)] = attrs;
    }];
    NSMutableDictionary *textStyleDic = [NSMutableDictionary dictionary];
    for (NSString *range in rangeDictionary) {
        
        id attributeDictionary = rangeDictionary[range];
        if ([attributeDictionary isKindOfClass:[NSDictionary class]]) {
            
            NSMutableDictionary *attributesForRange = [NSMutableDictionary dictionary];
            for (NSString *key in attributeDictionary) {
                
                id attributeObject = attributeDictionary[key];
                
                //color
                if ([attributeObject isKindOfClass:[UIColor class]]) {
                    
                    UIColor *color = attributeObject;
                    CGColorRef cgColor = [color CGColor];
                    if (cgColor) {
                        
                        size_t count = CGColorGetNumberOfComponents(cgColor);
                        const CGFloat *components = CGColorGetComponents(cgColor);
                        
                        NSMutableString *string = [NSMutableString string];
                        [string appendFormat:@"%f", components[0]];
                        for (int i = 1; i < count; i++) {
                            
                            [string appendFormat:@" %f", components[i]];
                        }
                        
                        attributesForRange[@"textColor"] = string;
                    } else {
                        
                        attributesForRange[@"textColor"] = @"0.0 0.0 0.0 0.0";
                    }
                }
                
                //font
                if ([attributeObject isMemberOfClass:[UIFont class]]) {
                    
                    UIFont *font = attributeObject;
                    attributesForRange[@"fontName"] = [font fontName];
                    attributesForRange[@"fontSize"] = @([[[font fontDescriptor] objectForKey:UIFontDescriptorSizeAttribute] doubleValue]);
                }
                
                //paragraph style
                if ([attributeObject isKindOfClass:[NSParagraphStyle class]]) {
                    
                    attributesForRange[@"paragraphStyle"] = [attributeObject description];
                }
            }
            textStyleDic[range] = attributesForRange;
        }
    }
    
    return textStyleDic;
}

+ (NSString *)GUID {
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    NSString *guid = (__bridge NSString *)string;
    CFRelease(string);
    return guid;
}

#pragma mark - Private methods

+ (const char *)getPropertyType:(objc_property_t)property {
    
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        
        if (attribute[0] == 'T') {
            
            if (strlen(attribute) <= 4) {
                
                break;
            }
            
            return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
        }
    }
    
    return "";
}

+ (void)grabButton:(UIButton *)object params:(NSMutableDictionary *)params {
    params[@"title"] = [object currentTitle];
}

+ (void)grabTextField:(UITextField *)object params:(NSMutableDictionary *)params {
    params[@"text"] = [object text];
}

+ (void)grabTextView:(UITextView *)object params:(NSMutableDictionary *)params {
    params[@"text"] = [object text];
}

+ (void)grabImageView:(UIImageView *)object params:(NSMutableDictionary *)params {
    params[@"size"] = NSStringFromCGSize([object.image size]);
}

//+ (void)grabBox:(NSBox *)object params:(NSMutableDictionary *)params {
//    params[@"title"] = [object title];
//}

//- (void)grabTableView:(UITableView *)tableView params:(NSMutableDictionary *)params {
//
//    if (tableView.dataSource) {
//        params[@"datasource"] = NSStringFromClass([tableView.dataSource class]);
//    }
//
//    NSInteger columns = tableView.numberOfColumns;
//    NSInteger rows = tableView.numberOfRows;
//    params[@"cols"] = @(columns);
//    params[@"rows"] = @(rows);
//
//    if (tableView.delegate) {
//        if ([tableView.delegate respondsToSelector:@selector(tableView:viewForTableColumn:row:)]) {
//            params[@"contentMode"] = @"View based";
//        } else {
//            params[@"contentMode"] = @"Cell based";
//        }
//        params[@"delegate"] = @{@"class":NSStringFromClass([tableView.delegate class]), @"methods":[CHIUIUtilities objectMethods:tableView.delegate]};
//
//        NSMutableArray *columnsArray = [NSMutableArray array];
//        if ([tableView.delegate respondsToSelector:@selector(tableView:objectValueForTableColumn:row:)]) {
//            for (int i =0; i < columns; i++) {
//                NSMutableArray *cells = [NSMutableArray array];
//                NSTableColumn *col = columnsArray[i];
//                for (int j =0; j < rows; j++) {
//                    id cell = [(id)tableView.delegate tableView:tableView objectValueForTableColumn:col row:j];
//                    [cells addObject:[cell description]];
//                }
//                [columnsArray addObject:cells];
//            }
//        }
//        params[@"cells"] = columnsArray;
//    }
//}

+ (UIFont *)loadFontWithFontName:(NSString *)fontName size:(double)fontSize path:(NSString *)fontPath {
    
    NSString *path = @"";//[[self.directoryPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:fontPath];
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithFilename([path UTF8String]);
    if (dataProvider) {
        CGFontRef fontRef = CGFontCreateWithDataProvider(dataProvider);
        if (fontRef) {
            CTFontRef fontCore = CTFontCreateWithGraphicsFont(fontRef, fontSize, NULL, NULL);
            CGFontRelease(fontRef);
            UIFont *font = (__bridge UIFont*)fontCore;
            return font;
        }
        CGDataProviderRelease (dataProvider);
    }
    return nil;
}

@end
