//
//  UIBezierPath+CHIAddition.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIBezierPath+CHIAddition.h"

// remove/comment out this line of you don't want to use undocumented functions
#define MCBEZIER_USE_PRIVATE_FUNCTION

#ifdef MCBEZIER_USE_PRIVATE_FUNCTION
extern CGPathRef CGContextCopyPath(CGContextRef context);
#endif

static void CGPathCallback(void *info, const CGPathElement *element)
{
    UIBezierPath *path = (__bridge UIBezierPath *)(info);
    CGPoint *points = element->points;
    
    switch (element->type) {
        case kCGPathElementMoveToPoint:
        {
            [path moveToPoint:CGPointMake(points[0].x, points[0].y)];
            break;
        }
        case kCGPathElementAddLineToPoint:
        {
            [path addLineToPoint:CGPointMake(points[0].x, points[0].y)];
            break;
        }
        case kCGPathElementAddQuadCurveToPoint:
        {
            // NOTE: This is untested.
            CGPoint currentPoint = [path currentPoint];
            CGPoint interpolatedPoint = CGPointMake((currentPoint.x + 2*points[0].x) / 3, (currentPoint.y + 2*points[0].y) / 3);
            [path addCurveToPoint:CGPointMake(points[1].x, points[1].y) controlPoint1:interpolatedPoint controlPoint2:interpolatedPoint];
            break;
        }
        case kCGPathElementAddCurveToPoint:
        {
            [path addCurveToPoint:CGPointMake(points[2].x, points[2].y) controlPoint1:CGPointMake(points[0].x, points[0].y) controlPoint2:CGPointMake(points[1].x, points[1].y)];
            break;
        }
        case kCGPathElementCloseSubpath:
        {
            [path closePath];
            break;
        }
    }
}

@implementation UIBezierPath (CHIAddition)

+ (UIBezierPath *)bezierPathWithCGPath:(CGPathRef)pathRef
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGPathApply(pathRef, (__bridge void * _Nullable)(path), CGPathCallback);
    
    return path;
}

@end
