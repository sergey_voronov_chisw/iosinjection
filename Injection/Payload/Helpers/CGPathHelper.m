//
//  CGPathHelper.m
//  iOSInjection
//
//  Created by userMacBookPro on 9/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "CGPathHelper.h"

static NSString * const CGPathInfoKeyType   = @"type";
static NSString * const CGPathInfoKeyPoints = @"points";

//--------------------------------------------------------------------------------------------------

static void pathApplyCallback(void* info, const CGPathElement* element) {
    NSMutableArray *pathInfoArray = (__bridge NSMutableArray *)info;
    
    int nPoints;
    BOOL isElementTypeExpected = YES;
    switch (element->type) {
        case kCGPathElementMoveToPoint:
        case kCGPathElementAddLineToPoint:
            nPoints = 1;
            break;
        case kCGPathElementAddQuadCurveToPoint:
            nPoints = 2;
            break;
        case kCGPathElementAddCurveToPoint:
            nPoints = 3;
            break;
        case kCGPathElementCloseSubpath:
            nPoints = 0;
            break;
        default:
            isElementTypeExpected = NO;
    }
    
    if (isElementTypeExpected == NO) {
        [pathInfoArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithBool:NO]];
    } else {
        NSNumber *type = [NSNumber numberWithInt:element->type];
        NSData   *points = [NSData dataWithBytes:element->points length:nPoints * sizeof(CGPoint)];
        [pathInfoArray addObject:@{CGPathInfoKeyType  : type,
                                   CGPathInfoKeyPoints: points
                                   }
         ];
    }
}

//--------------------------------------------------------------------------------------------------

@implementation CGPathHelper

#pragma mark - Public methods

- (instancetype)initWithPathRef:(CGPathRef)pathRef {
    self = [super init];
    if (self) {
        self.pathRef = pathRef;
    }
    return self;
}

- (NSArray *)pathArray {
    NSMutableArray *pathInfoArray = [NSMutableArray arrayWithObject:[NSNumber numberWithBool:YES]];
    CGPathApply(self.pathRef, (__bridge void * _Nullable)(pathInfoArray), pathApplyCallback);
    return pathInfoArray;
}

- (BOOL)setupPathRefWithArray:(NSArray *)array {
    __block BOOL result = YES;
    
    CGMutablePathRef pathRef = CGPathCreateMutable();
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj isKindOfClass:[NSDictionary class]]) {
            CGPoint *points = (CGPoint *)[[obj objectForKey:CGPathInfoKeyPoints] bytes];
            switch ([[obj objectForKey:CGPathInfoKeyType] intValue]) {
                case kCGPathElementMoveToPoint:
                    CGPathMoveToPoint(pathRef, NULL, points[0].x, points[0].y);
                    break;
                case kCGPathElementAddLineToPoint:
                    CGPathAddLineToPoint(pathRef, NULL, points[0].x, points[0].y);
                    break;
                case kCGPathElementAddQuadCurveToPoint:
                    CGPathAddQuadCurveToPoint(pathRef, NULL, points[0].x, points[0].y, points[1].x, points[1].y);
                    break;
                case kCGPathElementAddCurveToPoint:
                    CGPathAddCurveToPoint(pathRef, NULL, points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y);
                    break;
                case kCGPathElementCloseSubpath:
                    CGPathCloseSubpath(pathRef);
                    break;
                default:
                    *stop = YES;
                    result = NO;
            }
        };
    }];
    
    if (result == NO) {
        self.pathRef = pathRef;
    }
    
    CGPathRelease(pathRef);
    
    return result;
}

#pragma mark - Setter

- (void)setPathRef:(CGPathRef)pathRef {
    if (_pathRef != pathRef) {
        CGPathRelease(_pathRef);
        _pathRef = CGPathRetain(pathRef);
    }
}

#pragma mark - Memory management

- (void)dealloc {
    CGPathRelease(_pathRef);
//    [super dealloc];
}

@end
