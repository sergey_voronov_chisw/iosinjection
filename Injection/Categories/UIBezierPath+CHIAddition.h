//
//  UIBezierPath+CHIAddition.h
//  iOSInjection
//
//  Created by Sergey Voronov on 7/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBezierPath (CHIAddition)

+ (UIBezierPath*)bezierPathWithCGPath:(CGPathRef)pathRef;

@end
