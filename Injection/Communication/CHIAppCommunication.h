//
//  CHIAppCommunication.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 24.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHIAppClientServerRequest.h"
#import "ThoMoNetworking.h"

@interface CHIAppCommunication : NSObject </*ThoMoServerDelegateProtocol, */ThoMoClientDelegateProtocol>

@property (nonatomic, strong) NSMutableArray *queue;

+ (instancetype)sharedInstance;
- (void)start;
- (void)sendRequest:(CHIAppClientServerRequest *)request timeout:(double)timeout completion:(CHIRequestCompletion)completion;
- (void)sendRequest:(CHIAppClientServerRequest *)request timeout:(double)timeout clientId:(NSString *)clientId completion:(CHIRequestCompletion)completion;
- (void)sendRequestType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params completion:(CHIRequestCompletion)completion;
- (void)sendRequestType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params timeout:(double)timeout completion:(CHIRequestCompletion)completion;
- (void)sendRequestType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params timeout:(double)timeout clientId:(NSString *)clientId completion:(CHIRequestCompletion)completion;
- (NSString *)ipAddress;

@end
