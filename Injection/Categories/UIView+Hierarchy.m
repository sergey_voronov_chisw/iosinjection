//
//  UIView+Hierarchy.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIView+Hierarchy.h"
#import "NSObject+ClassName.h"

@implementation UIView (Hierarchy)

- (UIView *)enclosingViewOfClassFromArray:(NSArray<Class> *)classes {
    
    UIView *view = self;
    
    do {
        
        view = [view superview];
    } while (view && [view isKindOfClasses:classes] == NO);
    
    return view;
}

- (UIView *)enclosingViewOfClassName:(NSString *)className {
    
    UIView *view = self;
    
    do {
        
        view = [view superview];
    } while (view && [[view className] isEqualToString:className] == NO);
    
    return view;
}

- (BOOL)isKindOfClasses:(NSArray<Class> *)classes {
    
    __block BOOL result = NO;
    
    [classes enumerateObjectsUsingBlock:^(Class _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([self isKindOfClass:obj]) {
            
            *stop = YES;
            result = YES;
        }
    }];
    
    return result;
}

- (BOOL)isOfClassNames:(NSArray<NSString *> *)classNames {
    
    __block BOOL result = NO;
    
    [classNames enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([[self className] isEqualToString:obj]) {
            
            *stop = YES;
            result = YES;
        }
    }];
    
    return result;
}

- (NSString *)hierarchyPath {
    
    NSString *className = [self className];
    NSString *viewName =
        [NSString stringWithFormat:@"%@_%p",
            [className substringToIndex:MIN(className.length, 10)],
            self
         ];
//    [[className stringByAppendingString:[NSString stringWithFormat:@"_%@", [NSString stringWithFormat:@"%p", self]]];
//    [NSString stringWithFormat:@"%p", self];
    
    NSString *result;
    
    if (!self.superview) {
        
        if ([self isKindOfClass:[UIWindow class]]) {
            
            result = viewName;
        }
    } else {
        
        result = [[self.superview hierarchyPath] stringByAppendingPathComponent:viewName];
    }
    
    return result;
}

- (NSArray *)hideSubviewsIfNeeded {
    
    NSMutableArray<NSNumber *> *result = [NSMutableArray arrayWithCapacity:self.subviews.count];
    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [result addObject:[NSNumber numberWithBool:obj.isHidden]];
        if (obj.isHidden == NO) {
            
            [obj setHidden:YES];
        }
    }];
    
    return result;
}

- (void)showSubviewsIfNeeded:(NSArray<NSNumber *> *)subviewsHiddenStatuses {
    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        BOOL wasHidden = [[subviewsHiddenStatuses objectAtIndex:idx] boolValue];
        if (wasHidden == NO) {
            
            [obj setHidden:NO];
        }
    }];
}

#pragma mark - DEBUG

- (void)DEBUG_logSuperviews {
    
    UIView *view = self;
    
    do {
        
        NSLog(@"===== %s: %@ (%@)", __FUNCTION__, [view className], NSStringFromCGRect(view.frame));
        view = [view superview];
    } while (view);
}

- (void)DEBUG_logSubviews {
    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSLog(@"===== %s: %@ (%@)", __FUNCTION__, [obj className], NSStringFromCGRect(obj.frame));
    }];
}

@end
