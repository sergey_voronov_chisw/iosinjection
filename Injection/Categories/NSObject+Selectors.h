//
//  NSObject+Selectors.h
//  iOSInjection
//
//  Created by Sergey Voronov on 8/11/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Selectors)

+ (id)performSelectorWithName:(NSString *)selectorName andParams:(NSArray *)params ofObject:(id)object;
- (id)performSelectorWithName:(NSString *)selectorName andParams:(NSArray *)params;

//+ (id)valueForSelectorName:(NSString *)selectorName ofObject:(id)object;
+ (void)putValues:(NSArray *)values withSelectorName:(NSString *)selectorName toObject:(id)object;
//
//- (id)valueForSelectorName:(NSString *)selectorName;
- (void)putValues:(NSArray *)values withSelectorName:(NSString *)selectorName;

+ (void)swizzleOrigClass:(Class)origClass
                 origSel:(SEL)origSel
              patchClass:(Class)patchClass
                patchSel:(SEL)patchSel;

@end
