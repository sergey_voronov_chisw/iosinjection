//
//  CHIInjectedAppManager.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHIInjectedAppManager : NSObject

+ (void)start;
+ (void)createDirectoryPath:(NSString *)path;
+ (void)writeObject:(id)object path:(NSString *)path;
+ (void)copyItem:(NSString *)fromPath to:(NSString *)toPath;
+ (NSString *)serverFilePathForFontNameInBundle:(NSString *)fontName;

@end
