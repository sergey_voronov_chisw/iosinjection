//
//  AppDelegate.h
//  iOSInjection
//
//  Created by Sergey Voronov on 6/29/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

