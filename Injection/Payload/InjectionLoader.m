//
//  PatchLoader.m
//  iOSInjection
//
//  Created by Sergey Voronov on 6/29/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIViewController+TopmostViewController.h"
#import "CHIInjectedAppManager.h"

//--------------------------------------------------------------------------------------------------

void startAppManagerWithProtocolIdentifier(NSString *protocolIdentifier) {
    
    [[NSUserDefaults standardUserDefaults] setObject:protocolIdentifier forKey:@"ProtocolIdentifier"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    dispatch_async(dispatch_get_main_queue() /*dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_BACKGROUND)*/, ^{
        
        [CHIInjectedAppManager start];
    });
}

//--------------------------------------------------------------------------------------------------

void showAlertWithAppInfo(NSDictionary *appInfo) {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Congratulations!"
                                                                   message:[NSString stringWithFormat:@"Code injected!\n%@\nConnect to recorder with protocol identifier...", appInfo]
                                                            preferredStyle:UIAlertControllerStyleActionSheet/*UIAlertControllerStyleAlert*/];
    [alert addAction:[UIAlertAction actionWithTitle:@"Test"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action)
                      {
                          startAppManagerWithProtocolIdentifier(@"test");
                      }]
     ];
    [alert addAction:[UIAlertAction actionWithTitle:@"Test2"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action)
                      {
                          startAppManagerWithProtocolIdentifier(@"test2");
                      }]
     ];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleCancel
                                            handler:nil]
     ];
    
    id class = [UIViewController class];
    SEL selector = @selector(topmostViewControllerWithRootViewController:);
    if ([class respondsToSelector:selector]) {
        
//        NSLog(@"\n\n\n===== Code injected =====\n\nSelector found: %@\n\n\n ", NSStringFromSelector(selector));
        UIViewController *vc = [UIViewController topmostViewControllerWithRootViewController:nil];
        if (vc) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [vc presentViewController:alert animated:YES completion:nil];
            });
        }
    } else {
        
        NSLog(@"\n\n\n===== Code injected =====\n\nNot found selector: %@\n\n\n ", NSStringFromSelector(selector));
    }
}

//--------------------------------------------------------------------------------------------------

//static void __attribute__((constructor)) entryPoint(void) {
__attribute__((constructor))
void entryPoint() {
    
    NSLog(@"\n\n\n===== Code injected =====\n\n%@\n\n\n ", [NSBundle mainBundle].bundleIdentifier);
    NSString *protocolIdentifier = [[NSUserDefaults standardUserDefaults] stringForKey:@"ProtocolIdentifier"];
    if (!protocolIdentifier.length) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5. * NSEC_PER_SEC)), dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_BACKGROUND)/*dispatch_get_main_queue()*/, ^{
            
//        dispatch_async(dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_BACKGROUND), ^{
            NSString *rootVCClassName = NSStringFromClass([[UIApplication sharedApplication].keyWindow.rootViewController class]);
            NSLog(@"\n\n\n===== Code injected =====\n\nRoot VC: %@\n\n\n ", rootVCClassName);
            showAlertWithAppInfo(@{@"Bundle ID": [NSBundle mainBundle].bundleIdentifier,
                                   @"Root VC"  : rootVCClassName}
                                 );
        });
    } else {
        
        startAppManagerWithProtocolIdentifier(protocolIdentifier);
    }
}

//--------------------------------------------------------------------------------------------------
