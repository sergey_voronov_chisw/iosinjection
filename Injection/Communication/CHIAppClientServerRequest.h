//
//  CHIAppClientServerRequest.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define kRequestTypeUncknown @"Uncknown"
#define kRequestTypeResponse @"Response"
#define kRequestTypeReuqest  @"Reuqest"
#define kRequestTypeCommand  @"Command"
#define kRequestTypeBridge   @"Bridge"
#define kRequestTypeStatus   @"Status"

#define kRequestNameIsAlive          @"IsAlive"
#define kRequestNameIsReady          @"IsReady"
#define kRequestCreateProjectPath    @"CreateProjectPath"
#define kRequestNameFinishAnalyze    @"FinishAnalyze"
#define kRequestNameFinishGrab       @"FinishGrab"
#define kRequestNameAnalyze          @"Analyze"
#define kRequestNameAnalyzeIgnored   @"AnalyzeIgnored"
#define kRequestNameGrab             @"Grab"
#define kRequestNameRoguesFinishGrab @"RoguesFinishGrab"
#define kRequestNameProcessVideo     @"ProcessVideo"
#define kRequestNameStopVideo        @"StopVideo"
#define kRequestNameDisableClient    @"DisableClient"
#define kRequestNameEnableClient     @"EnableClient"
#define kRequestNameStopRecording    @"StopRecording"
#define kRequestNameCreateDirectory  @"CreateDirectory"
#define kRequestNameWriteData        @"WriteData"
#define kRequestNameCopyItem         @"CopyItem"

typedef void(^CHIRequestCompletion)(id result, NSError *error);

@interface CHIAppClientServerRequest : NSObject

@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic) NSMutableArray *clients;

+ (instancetype)requestWithType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params;
+ (instancetype)requestFromJSONString:(NSString *)JSONString;
- (NSString *)JSONString;

@end
