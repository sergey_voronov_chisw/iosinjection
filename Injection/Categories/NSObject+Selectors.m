//
//  NSObject+Selectors.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/11/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "NSObject+Selectors.h"
#import <objc/runtime.h> // for swizzling

typedef id   (*InvokeMethod           )(id, SEL        );
typedef id   (*InvokeMethodWith1Param )(id, SEL, id    );
typedef id   (*InvokeMethodWith2Params)(id, SEL, id, id);

//typedef id   (*GetterMethod)(id, SEL);
typedef void (*SetterMethod)(id, SEL, id);
typedef void (*LaunchMethodWith2Params)(id, SEL, id, id);

@implementation NSObject (Selectors)

+ (id)performSelectorWithName:(NSString *)selectorName andParams:(NSArray *)params ofObject:(id)object {
    
    id result;
    
    if (object) {
        SEL selector = NSSelectorFromString(selectorName);
        if ([object respondsToSelector:selector]) {
            switch (params.count) {
                case 0: {
                    InvokeMethod method = (InvokeMethod)[object methodForSelector:selector];
                    result = (method)(object, selector);
                }
                    break;
                    
                case 1: {
                    InvokeMethodWith1Param method = (InvokeMethodWith1Param)[object methodForSelector:selector];
                    result = (method)(object, selector, params[0]);
                }
                    break;
                    
                case 2: {
                    InvokeMethodWith2Params method = (InvokeMethodWith2Params)[object methodForSelector:selector];
                    result = (method)(object, selector, params[0], params[1]);
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    return result;
}

- (id)performSelectorWithName:(NSString *)selectorName andParams:(NSArray *)params {
    return [NSObject performSelectorWithName:selectorName andParams:params ofObject:self];
}


//+ (id)valueForSelectorName:(NSString *)selectorName ofObject:(id)object {
//    id result;
//    
//    if (object) {
//        SEL selector = NSSelectorFromString(selectorName);
//        if ([object respondsToSelector:selector]) {
//            GetterMethod method = (GetterMethod)[object methodForSelector:selector];
//            result = (method)(object, selector);
//        }
//    }
//    
//    return result;
//}
//
+ (void)putValues:(NSArray *)values withSelectorName:(NSString *)selectorName toObject:(id)object {
    if (object) {
        SEL selector = NSSelectorFromString(selectorName);
        if ([object respondsToSelector:selector]) {
            switch (values.count) {
                case 1: {
                    SetterMethod method = (SetterMethod)[object methodForSelector:selector];
                    (method)(object, selector, values[0]);
                }
                    break;
                    
                case 2: {
                    LaunchMethodWith2Params method = (LaunchMethodWith2Params)[object methodForSelector:selector];
                    (method)(object, selector, values[0], values[1]);
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
}

//- (id)valueForSelectorName:(NSString *)selectorName {
//    return [NSObject valueForSelectorName:selectorName ofObject:self];
//}
//
- (void)putValues:(NSArray *)values withSelectorName:(NSString *)selectorName {
    [NSObject putValues:values withSelectorName:selectorName toObject:self];
}

//- (void)valueForSelector:(SEL)selector
//                  result:(void *)result
//          argumentsBlock:(void(^)(NSInvocation *))argumentsBlock {
//    
//    NSMethodSignature *signature = [self.class instanceMethodSignatureForSelector:selector];
//    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
//    [invocation setSelector:selector];
//    [invocation setTarget:self];
//    if (argumentsBlock) {
//        
//        argumentsBlock(invocation);
//    }
//    [invocation invoke];
//    [invocation getReturnValue:result];
//}
//

+ (void)swizzleOrigClass:(Class)origClass
                 origSel:(SEL)origSel
              patchClass:(Class)patchClass
                patchSel:(SEL)patchSel {
    
    Method patchMethod = class_getInstanceMethod(patchClass, patchSel);
    IMP patchImplementation = method_getImplementation(patchMethod);
    BOOL didMethodAdded = class_addMethod(origClass, patchSel, patchImplementation, method_getTypeEncoding(patchMethod));
    if (didMethodAdded) {
        
        Method origMethod = class_getInstanceMethod(origClass, origSel);
        if (origMethod) {
            
            Method newMethod = class_getInstanceMethod(origClass, patchSel);
            method_exchangeImplementations(origMethod, newMethod);
        }
    }
}

@end
