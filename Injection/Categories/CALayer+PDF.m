//
//  CALayer+PDF.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/17/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "CALayer+PDF.h"
#import <UIKit/UIKit.h> // for UIGraphics...

@implementation CALayer (PDF)

- (NSData *)pdfDataWithDataConsumer {
//    NSURL *fileURL = [NSURL fileURLWithPath:[self.writePath stringByAppendingPathComponent:kPdfFileName]];
    
    // Initialize
    CFMutableDataRef pdfDataRef = CFDataCreateMutable(NULL, 0);
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData(pdfDataRef);
    CGRect mediaBox = self.bounds;
    CGContextRef context = CGPDFContextCreate(dataConsumer, &mediaBox, NULL);
    //CGContextRef context = CGPDFContextCreateWithURL((__bridge CFURLRef)(fileURL), &mediaBox, NULL);
    
    // Render
    CGPDFContextBeginPage(context, nil);
    [self renderInContext:context];
    CGPDFContextEndPage(context);
    
    // Finalize
    CGPDFContextClose(context);
    CGContextRelease(context);
    
    return ((__bridge NSMutableData *)pdfDataRef);
}

- (NSData *)pdfDataWithDataConsumerWithSublayersToHide:(NSArray *)sublayersToHide {
    
    // Initialize
    CFMutableDataRef pdfDataRef = CFDataCreateMutable(kCFAllocatorDefault, 0);
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData(pdfDataRef);
    
    // mark the PDF as coming from the client (not from a web-site)
//    CFMutableDictionaryRef auxInfo = CFDictionaryCreateMutable(kCFAllocatorDefault, 1, NULL, NULL);
//    CFDictionaryAddValue(auxInfo, kCGPDFContextCreator, CFSTR("LayersRecorder"));
//    CFDictionaryRef auxillaryInformation = CFDictionaryCreateCopy(kCFAllocatorDefault, auxInfo);
//    CFRelease(auxInfo);
    
    CGRect mediaBox = self.bounds;
    CGContextRef context = CGPDFContextCreate(dataConsumer, &mediaBox, NULL/*auxillaryInformation*/);
//    CFRelease(auxillaryInformation);
    CGDataConsumerRelease(dataConsumer);
    
    // Render
    CGPDFContextBeginPage(context, nil);
    
    NSMutableDictionary *hiddenState = [NSMutableDictionary dictionary];
    for (CALayer *sublayer in self.sublayers) { // Hide sublayers
        NSInteger index = [sublayersToHide indexOfObject:sublayer];
        if (index == NSNotFound) {
            hiddenState[[NSString stringWithFormat:@"%p", sublayer]] = @(sublayer.hidden);
            sublayer.hidden = YES;
        }
    }
    BOOL maskHidden = self.mask.hidden;
    self.mask.hidden = YES;
    [self renderInContext:context];
    self.mask.hidden = maskHidden;
    
    for (CALayer *sublayer in self.sublayers) { // Show hidden sublayers
        NSInteger index = [sublayersToHide indexOfObject:sublayer];
        if (index == NSNotFound) {
            sublayer.hidden = [hiddenState[[NSString stringWithFormat:@"%p", sublayer]] boolValue];
        }
    }
    
    CGPDFContextEndPage(context);
    
    // Finalize
    CGContextFlush(context);
    CGPDFContextClose(context);
    CGContextRelease(context);
    
    NSData *result = CFBridgingRelease(pdfDataRef);
    return result;
}

- (NSData *)pdfData/*InsideRect:(CGRect)frame*/ {
    NSMutableData *pdfData = [NSMutableData data];
//    CGRect bounds = (CGRect){.origin = CGPointZero, .size = kPaperSizeA4};
    
    UIGraphicsBeginPDFContextToData(pdfData, self.bounds, nil);
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    UIGraphicsBeginPDFPage();
    [self renderInContext:pdfContext];
    
    UIGraphicsEndPDFContext();
    
    return pdfData;
}

- (NSData *)pngData {
    return UIImagePNGRepresentation([self image]);
}

//- (void)saveAsPDFWithPath:(NSString *)path {
//
//    NSURL *fileURL = [NSURL fileURLWithPath:path];
//    CGRect mediaBox = self.bounds;
//
//    CGContextRef pdfContext = CGPDFContextCreateWithURL((__bridge CFURLRef)(fileURL), &mediaBox, NULL);
//    CGPDFContextBeginPage(pdfContext, nil);
//    [self renderInContext:pdfContext];
//
//    CGPDFContextEndPage(pdfContext);
//    CGPDFContextClose(pdfContext);
//    CGContextRelease(pdfContext);
//}
//
//- (void)pdfTest:(NSString *)path {
//    CGContextRef aCgPDFContextRef = [self createPDFContext:[/*[*/self.target.window/*.contentView superview]*/ bounds]
//                                                      path:(__bridge CFStringRef)path]];
//    
//    CGContextBeginPage(aCgPDFContextRef,nil);
//    
//    //Draw view into PDF
//    UIView *aPageNote = /*[*/self.target.window/*.contentView superview]*/;
//    
//    CGAffineTransform aCgAffTrans = CGAffineTransformIdentity;
//    aCgAffTrans = CGAffineTransformMakeTranslation(0, 892);
//    aCgAffTrans = CGAffineTransformScale(aCgAffTrans, 1.0, -1.0);
//    //CGContextConcatCTM(aCgPDFContextRef, aCgAffTrans);
//    
////    [aPageNote setWantsLayer:YES];
//    [aPageNote.layer renderInContext:aCgPDFContextRef];
////    UIImage *image = [[UIImage alloc] init]; //initWithSize:aPageNote.bounds.size];
////    [image lockFocus];
//    
//    [aPageNote drawRect:[aPageNote bounds]];
////    [image unlockFocus];
//    
//    CGContextEndPage(aCgPDFContextRef);
//    CGContextRelease (aCgPDFContextRef);
//    
//}
//
#pragma mark - Private method

- (UIImage *)image {
    
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0);
    
    [self renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

//- (CGContextRef)createPDFContext:(CGRect)aCgRectinMediaBox path:(CFStringRef) aCfStrPath {
//    CGContextRef aCgContextRefNewPDF = NULL;
//    CFURLRef aCfurlRefPDF = CFURLCreateWithFileSystemPath(NULL, aCfStrPath, kCFURLPOSIXPathStyle, false);
//    if (aCfurlRefPDF != NULL) {
//        aCgContextRefNewPDF = CGPDFContextCreateWithURL(aCfurlRefPDF, &aCgRectinMediaBox, NULL);
//        CFRelease(aCfurlRefPDF);
//    }
//    return aCgContextRefNewPDF;
//}
//
@end
