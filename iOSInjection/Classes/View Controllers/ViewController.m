//
//  ViewController.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/5/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "ViewController.h"
#import "CHIInjectedAppManager.h"
//#import "Constants.h"
//#import "CHIAppClientServerRequest.h"

@interface ViewController ()

// XIB
@property (weak, nonatomic) IBOutlet UILabel *label;

// Internal
//@property (nonatomic) NSTimer *timer;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [CHIInjectedAppManager start];
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        // Simulate creating recorder's project path
//        NSDictionary *params = @{@"path": @"test_path"};
//        NSDictionary *requestInfo = @{@"type"  : kRequestTypeCommand,
//                                      @"name"  : kRequestCreateProjectPath,
//                                      @"params": params
//                                      };
//        NSDictionary *userInfo = @{ kNotificationUserInfoKeyRequest: requestInfo };
//        [[NSNotificationCenter defaultCenter] postNotificationName:kClientNotification
//                                                            object:nil
//                                                          userInfo:userInfo];
//        
//        
//        // Simulate start recording (event sent from the recorder by user action) and grabbing changes
//        params = @{@"objectName": @"All",
//                   @"time"      : @0
//                   };
//        requestInfo = @{@"type"  : kRequestTypeCommand,
//                        @"name"  : kRequestNameGrab,
//                        @"params": params
//                        };
//        userInfo = @{ kNotificationUserInfoKeyRequest: requestInfo};
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.
//                                                      target:self
//                                                    selector:@selector(didTimerTriggerred:)
//                                                    userInfo:userInfo
//                                                     repeats:YES];
//    });
}

//- (void)didTimerTriggerred:(NSTimer *)sender {
//    [[NSNotificationCenter defaultCenter] postNotificationName:kClientNotification
//                                                        object:nil
//                                                      userInfo:sender.userInfo];
//}
//
- (IBAction)didButtonTap:(id)sender {
    CGFloat red = 0., green = 0., blue = 0., alpha = 0.;
    [self.label.textColor getRed:&red green:&green blue:&blue alpha:&alpha];
//    if (red != 1.) {
//        red   = 1.;
//        green = 0.;
//        blue  = 0.;
//    } else if (green != 1.) {
//        red   = 0.;
//        green = 1.;
//        blue  = 0.;
//    } else if (blue != 1.) {
//        red   = 0.;
//        green = 0.;
//        blue  = 1.;
//    }
    if (red == 1.) {
        red   = 0.;
        green = 1.;
        blue  = 0.;
    } else if (green == 1.) {
        red   = 0.;
        green = 0.;
        blue  = 1.;
    } else {
        red   = 1.;
        green = 0.;
        blue  = 0.;
    }
    self.label.textColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
