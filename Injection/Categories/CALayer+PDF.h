//
//  CALayer+PDF.h
//  iOSInjection
//
//  Created by Sergey Voronov on 8/17/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (PDF)

- (NSData *)pdfDataWithDataConsumer;
- (NSData *)pdfDataWithDataConsumerWithSublayersToHide:(NSArray *)sublayersToHide;
- (NSData *)pdfData;
- (NSData *)pngData;

@end
