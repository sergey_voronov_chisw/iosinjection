//
//  UIViewController+TopmostViewController.h
//  iOSInjection
//
//  Created by Sergey Voronov on 8/11/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TopmostViewController)

+ (UIViewController*)topmostViewControllerWithRootViewController:(UIViewController*)rootViewController;

@end
