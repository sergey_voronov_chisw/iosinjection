//
//  NSBundle+Fonts.m
//  iOSInjection
//
//  Created by userMacBookPro on 8/29/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "NSBundle+Fonts.h"

@implementation NSBundle (Fonts)

- (NSArray *)fonts {
    NSMutableArray *result = [NSMutableArray new];
    
    NSArray *fonts = [self fontsInFolder:@"fonts"];
    if (fonts.count) {
        [result addObjectsFromArray:fonts];
    }
    fonts = [self fontsInFolder:nil];
    if (fonts.count) {
        [result addObjectsFromArray:fonts];
    }
    
    return result;
}

- (NSArray *)fontsInFolder:(NSString *)folder {
    __block NSMutableArray *result = [NSMutableArray new];
    
    NSString *filesPath;
    if (folder.length) {
        filesPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:folder];
    } else {
        filesPath = [[NSBundle mainBundle] resourcePath];
    }
    NSURL *filesURL = [NSURL fileURLWithPath:filesPath];
    if (filesURL != nil) {
        NSError *error;
        NSArray<NSURL *> *files =
            [[NSFileManager defaultManager] contentsOfDirectoryAtURL:filesURL
                                          includingPropertiesForKeys:nil
                                                             options:NSDirectoryEnumerationSkipsHiddenFiles
                                                               error:&error];
        if (!error && files.count) {
            [files enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([[obj pathExtension] isEqualToString:@"ttf"]) {
                    [result addObject:obj];
                }
            }];
        }
    }
    
    return result;
}

@end
