//
//  UIColor+CHIColorStore.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIColor+CHIColorStore.h"

@implementation UIColor (CHIColorStore)

+ (NSString *)stringRepresentationFromCGColor:(CGColorRef)color {
    CGColorSpaceRef colorSpace = CGColorGetColorSpace(color);
    CGColorSpaceModel model = CGColorSpaceGetModel(colorSpace);
    const CGFloat* components = CGColorGetComponents(color);
    switch (model) {
        case kCGColorSpaceModelRGB: {
            return [NSString stringWithFormat:@"rgb:%f %f %f %f", components[0], components[1], components[2], CGColorGetAlpha(color)];
        } break;
            
        case kCGColorSpaceModelIndexed: {
            return [NSString stringWithFormat:@"index:%f", components[0]];
        } break;
            
        case kCGColorSpaceModelMonochrome: {
            return [NSString stringWithFormat:@"mono:%f %f", components[0], CGColorGetAlpha(color)];
        } break;
            
        default: {
            return [NSString stringWithFormat:@"undef: %d", model];
        } break;
    }
}

- (NSString *)stringRepresentation {
    return [UIColor stringRepresentationFromCGColor:self.CGColor];
}

@end
