//
//  CHIAODesktopIcon.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 21.09.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAODesktopIconTitle.h"
#import "Constants.h"
#import "CHIUIUtilities.h"
#import "NSObject+Selectors.h"

@implementation CHIAODesktopIconTitle

- (void)setupObserver {
    
    [super setupObserver];
    
    [self addObserverForKeyPath:@"attrString"];
    [self addObserverForKeyPath:@"titleFrame"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    
    if ([keyPath isEqualToString:@"attrString"] || [keyPath isEqualToString:@"titleFrame"]) {
        
        self.wasChanges |= kChangeText;
    }
}

- (void)grabTextWithInfo:(NSMutableDictionary *)info {
    
    NSAttributedString *arrtString = [self.target performSelectorWithName:@"attrString" andParams:nil];
    info[AOInfo.text] = [arrtString string];
    info[AOInfo.type] = AOInfoType.text;
}

- (void)grabTextStyleWithInfo:(NSMutableDictionary *)info {
    
    NSAttributedString *arrtString = [self.target performSelectorWithName:@"attrString" andParams:nil];
    info[AOInfo.textStyle] = [self textStyleForAttributedString:arrtString];
}

@end
