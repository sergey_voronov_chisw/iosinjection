//
//  CHIUIUtilities.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 20.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CHIUIUtilities : NSObject

+ (NSDictionary *)objectMethods:(id)object;
+ (NSArray *)objectMethodsNames:(id)object;
+ (NSDictionary *)objectProperties:(id)object;
+ (NSArray *)objectPropertiesNames:(id)object;
//+ (NSArray *)layersForObject:(id)object;
//+ (NSMutableDictionary *)grabObject:(id)object;
+ (NSString *)superclassFromClassPath:(NSString *)classPath ofClass:(Class)class;
//+ (UIFont *)fontFromTextStyleDictionary:(NSDictionary *)textStyle;
+ (NSDictionary *)textStyleForAttributedString:(NSAttributedString *)attributedString;
+ (NSString *)GUID;

@end
