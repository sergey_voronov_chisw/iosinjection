//
//  CHIAssociatedObject.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 25.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAssociatedObject.h"
#import <objc/runtime.h>
#import <objc/message.h>
#import <CoreText/CoreText.h>

#import "CHIUIUtilities.h"
#import "CHIWindowAnalyzer.h"
#import "CHIInjectedAppManager.h"
#import "Constants.h"
#import "CHIAppClient.h"
#import "NSObject+ClassName.h"
#import "UIView+VisibleRect.h"
#import "UIView+Hierarchy.h"
#import "CALayer+PDF.h"
#import "NSObject+ConvertPoints.h"
#import "NSObject+Selectors.h"
#import "Constants.h"

#import "UIColor+CHIColorStore.h"

@interface CHIAssociatedObject ()

@property (nonatomic, strong) NSMutableArray *_keys;
@property (nonatomic) BOOL _targetHidden;
@property (nonatomic, strong) NSNumber *_isSelected;

@end

@implementation CHIAssociatedObject

- (instancetype)initWithTargetObject:(UIView *)object path:(NSString *)path {
    
    self = [super init];
    
    if (self) {
        
        self.target = object;
        self.targetName = [NSString stringWithFormat:@"%p", self.target];
        [self resetWithPath:path];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(someFrameWasChanged:)
                                                     name:kGrameChangedNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(notificationFromServer:)
                                                     name:[NSString stringWithFormat:@"grabTarget_%@", self.targetName]
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(notificationFromServer:)
                                                     name:[NSString stringWithFormat:kGrabTargetAllNotification]
                                                   object:nil];
//        NSLog(@"===== ASSOCIATED OBJECT subscribed on grab notification %@", self);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(targetDeallocNotification:)
                                                     name:[NSString stringWithFormat:kNotificationUIViewDealloc]
                                                   object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(resetAONotification:)
//                                                     name:[NSString stringWithFormat:kNotificationResetAO]
//                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(deleteAONotification:)
                                                     name:[NSString stringWithFormat:kNotificationDeleteAO]
                                                   object:nil];

        objc_setAssociatedObject(object, kAssociatedObjectName, self, OBJC_ASSOCIATION_RETAIN);
        
        [self setupObserver];
    }

    return self;
}

- (BOOL)isTargetHidden {
    
    if ([self.target respondsToSelector:@selector(isHidden)]) {
        
        return [self.target isHidden];
    } else if ([self.target respondsToSelector:@selector(isVisible)]) {
        
        return ![(id)self.target isVisible];
    }
    
    return NO;
}

- (void)resetWithPath:(NSString *)path {
    
    self._keys = [NSMutableArray array];
    self._targetHidden = [self isTargetHidden];
    self.projectRootPath = path;
    self.className = [CHIUIUtilities superclassFromClassPath:self.target.className
                                                     ofClass:[self.target class]
                      ];
    self.lastScreenFrame = [self.target frame];
    self.lastFrame = [self.target frame];
    self.lastVisibleRenderFrameName = @"None";
    self.lastChangesFrameName = @"None";
    self.wasRendered = NO;
    self.margins = CGRectMake(0, 0, 0, 0);
    self.isOnScreen = YES;
    self.sendNotificationToServerOnDealloc = nil;
    self.superViewPath = [self.target hierarchyPath];
    self.createDate = [NSDate date];
    self._isSelected = nil;
    self.wasChanges = kChangeVisibleState | kChangeFrame | kChangeText;
}

- (void) forceFrameChanging:(NSNotification *)notification {
    
    self.wasChanges |= kChangeFrame | kChangeText;
}

- (void) someFrameWasChanged:(NSNotification *)notification {
    
    if (notification.userInfo[@"view"] == [self targetSuperview]) {
        
        self.wasChanges |= kChangeFrame | kChangeText;
    }
}

- (void)addObserverForKeyPath:(NSString *)keyPath {
    
    [self.target addObserver:self
                  forKeyPath:keyPath
                     options:NSKeyValueObservingOptionNew
                     context:nil];
    
    [self._keys addObject:keyPath];
}

- (void)setupObserver {
    
    if ([self.target respondsToSelector:@selector(isHidden)]) {
        
        [self addObserverForKeyPath:@"hidden"];
    }
    
    [self addObserverForKeyPath:@"frame"];
    
    if ([self.target isKindOfClass:[UIControl class]]) {
        
        [self addObserverForKeyPath:@"enabled"];
        [self addObserverForKeyPath:@"highlighted"];
    }
    
    if ([self.target respondsToSelector:@selector(state)]) {
        
        [self addObserverForKeyPath:@"state"];
    }
    
    self._isSelected = nil;
    if ([self.target respondsToSelector:@selector(isSelected)]) {
        
        self._isSelected = @([(id)self.target isSelected]);
    }
    
    self.wasChanges = kChangeVisibleState | kChangeFrame | kChangeText;
}

//- (void)mouseEntered:(UIEvent *)theEvent {
//    
//    self.wasChanges |= kChangeVisibleState | kChangeText;
//}
//
//- (void)mouseMoved:(UIEvent *)theEvent {
//    
//    self.wasChanges |= kChangeVisibleState | kChangeText;
//}
//
//- (void)mouseExited:(UIEvent *)theEvent {
//    
//    self.wasChanges |= kChangeVisibleState | kChangeText;
//}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    if ([keyPath isEqualToString:@"hidden"]) {
        
        BOOL hidden = [self isTargetHidden];
        if (hidden != self._targetHidden) {
            
            self._targetHidden = hidden;
            self.wasChanges |= kChangeFrame | kChangeText;
        }
    } else if ([keyPath isEqualToString:@"font"]) {
        
        self.wasChanges |= kChangeText;
    } else if ([keyPath isEqualToString:@"enabled"]        ||
               [keyPath isEqualToString:@"highlighted"]    ||
               [keyPath isEqualToString:@"image"]          ||
               [keyPath isEqualToString:@"alternateImage"] ||
               [keyPath isEqualToString:@"state"]           ) {
        
        self.wasChanges |= kChangeVisibleState;
    } else if ([keyPath isEqualToString:@"frame"]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kGrameChangedNotification
                                                            object:nil
                                                          userInfo:@{ @"view": self.target }
         ];
    }
}

- (BOOL)shouldGrab {
    
    if ([self superviewPathChanged]) {
        
        self.wasChanges |= kChangeText | kChangeFrame | kChangeVisibleState;
    }
    
    if (!self.superViewPath) {
        
        return NO;
    }
    
    CGRect frame = [self convertedFrame];
    CGRect frameRelativeToWindow = [self.target convertRect:[self.target bounds] toView:nil];

    BOOL outOfBounds = (frameRelativeToWindow.origin.x > self.target.window.frame.size.width ||
                        frameRelativeToWindow.origin.y > self.target.window.frame.size.height ||
                        frameRelativeToWindow.origin.x + frameRelativeToWindow.size.width < 0 ||
                        frameRelativeToWindow.origin.y + frameRelativeToWindow.size.height < 0);
    if ((self.isOnScreen && outOfBounds) || (!self.isOnScreen && !outOfBounds)) {
        
        self.wasChanges |= kChangeFrame;
    }

    if (!CGRectEqualToRect(self.lastFrame, frame)) { // Если фрейм изменился (!Equal)
        
        self.wasChanges |= kChangeFrame;
        if (!CGSizeEqualToSize(self.lastFrame.size, frame.size)) { // Если размер изменился
            
            self.wasChanges |= kChangeVisibleState; // Надо перерендерить (иначе остаются только параметрические изменения фрейма)
        }
        self.lastFrame = frame;
    }
    
    if (!CGRectEqualToRect(self.lastVisibleRect, self.target.visibleRect)) {
        
        self.lastVisibleRect = self.target.visibleRect;
        self.wasChanges |= kChangeFrame;
    }

    if (self._isSelected) {
        
        NSNumber *selected = @([(id)self.target isSelected]);
        if (![selected isEqualToNumber:self._isSelected]) {
            
            self._isSelected = selected;
            self.wasChanges |= kChangeVisibleState | kChangeText;
        }
    }
    
    return self.wasChanges != kChangeNone;
}

- (BOOL)superviewPathChanged {
    
    NSString *targetPath = [self.target hierarchyPath];
    
    BOOL result = NO;
    
    if (self.superViewPath) {
        
        if (![self.superViewPath isEqualToString:targetPath] /*&&
        [[self.superViewPath componentsSeparatedByString:@"/"] count] == [[targetSuperviewPath componentsSeparatedByString:@"/"] count]*/) {
        
            result = YES;
        }
    } else if (targetPath) {
        
        result = YES;
    }
    
    if (result) {
        
        if (!targetPath) {
            
            [self rip];
        }
        self.superViewPath = targetPath;
    }
    
    return result;
}

- (void)grabTarget {
    
    if (!self.target) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName: kAOGrabFinishNotification
                                                            object: nil
                                                          userInfo: @{ kNotificationParamFrameName: self.frameName }];
        return;
    }
    
    if ([self shouldGrab]) {
        
        NSUInteger flags = self.wasChanges;
        self.wasChanges = kChangeNone; // Сброс флагов в этом месте нужен на случай, если во время рендера будут установлены новые флаги (во время рендера будем юзать flags вместо wasChanges)
        
        [self createNewWritePath];

        NSMutableDictionary *info = [NSMutableDictionary dictionary];
        info[AOInfo.class] = self.className;
        info[AOInfo.frameName] = self.frameName;
        BOOL hidden = [self isTargetHidden];
        info[AOInfo.visible] =  [@(!hidden && !CGRectEqualToRect([self.target visibleRect], CGRectZero)) stringValue];
        info[AOInfo.zIndex] = @(self.zIndex);
        
        if (flags & kChangeVisibleState) {
            
            [self grabVisibleWithInfo:info];
        }

        if (flags & kChangeFrame) {
            
            [self grabFrameWithInfo:info];
        }
        
        if (flags & kChangeText) {
            
            [self grabTextWithInfo:info];
            [self grabTextStyleWithInfo:info];
        }

        info[AOInfo.lastVisibleRenderFrameName] = self.lastVisibleRenderFrameName;
        info[AOInfo.lastChangesFrameName] = self.lastChangesFrameName;
        
        if ([self.target isKindOfClass:[UITextField class]] ||
            [self.target isKindOfClass:[UITextView class]] ||
            [self.target isKindOfClass:[UIButton class]] ||
            [self.target isKindOfClass:[UILabel class]]) {
            
            info[AOInfo.type] = AOInfoType.apartText;
            info[AOInfo.shouldParseText] = @NO;
        }
        
        [CHIInjectedAppManager writeObject:info path:[self.writePath stringByAppendingPathComponent:kPlistFileName]];
        self.lastChangesFrameName = self.frameName;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName: kAOGrabFinishNotification
                                                        object: nil
                                                      userInfo: @{ kNotificationParamFrameName: self.frameName }];
}

- (void)grabTextWithInfo:(NSMutableDictionary *)info {
    
}

- (void)grabTextStyleWithInfo:(NSMutableDictionary *)info {
    
}

- (void)grabFont:(UIFont *)originalFont withInfo:(NSMutableDictionary *)info {
    
    NSString *fontServerFilePath = [CHIInjectedAppManager serverFilePathForFontNameInBundle:originalFont.fontName];
    if (fontServerFilePath.length) {
        
        info[AOInfo.fontPath] = fontServerFilePath;
    }
}

- (NSDictionary *)textStyleForAttributedString:(NSAttributedString *)attributedString {
    
    NSMutableDictionary *rangeDictionary = [NSMutableDictionary dictionary];
    [attributedString enumerateAttributesInRange:NSMakeRange(0, attributedString.length)
                                         options:0
                                      usingBlock:^(NSDictionary<NSString *,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
        rangeDictionary[NSStringFromRange(range)] = attrs;
    }];
    
    NSMutableDictionary *textStyleDic = [NSMutableDictionary dictionary];
    for (NSString *range in rangeDictionary) {
        
        id attributeDictionary = rangeDictionary[range];
        if ([attributeDictionary isKindOfClass:[NSDictionary class]]) {
            
            NSMutableDictionary *attributesForRange = [NSMutableDictionary dictionary];
            for (NSString *key in attributeDictionary) {
                
                id attributeObject = attributeDictionary[key];
                
                // color
                if ([attributeObject isKindOfClass:[UIColor class]]) {
                    
                    UIColor *color = attributeObject;
                    CGColorRef cgColor = [color CGColor];
                    if (cgColor) {
                        
                        size_t count = CGColorGetNumberOfComponents(cgColor);
                        const CGFloat *components = CGColorGetComponents(cgColor);
                        
                        NSMutableString *string = [NSMutableString string];
                        [string appendFormat:@"%f", components[0]];
                        for (int i = 1; i < count; i++) {
                            
                            [string appendFormat:@" %f", components[i]];
                        }
                        
                        attributesForRange[@"textColor"] = string;
                    } else {
                        
                        attributesForRange[@"textColor"] = @"0.0 0.0 0.0 0.0";
                    }
                }
                
                //font
                if ([attributeObject isKindOfClass:[UIFont class]]) {
                    
                    UIFont *font = attributeObject;
                    attributesForRange[@"fontName"] = [font fontName];
                    attributesForRange[@"fontSize"] = @([[[font fontDescriptor] objectForKey:UIFontDescriptorSizeAttribute] doubleValue]);
                    [self grabFont:font withInfo:attributesForRange];
                }
                
                //paragraph style
                if ([attributeObject isKindOfClass:[NSParagraphStyle class]]) {
                    
                    NSMutableParagraphStyle *attribute = [NSMutableParagraphStyle new];
                    [attribute setParagraphStyle:attributeObject];
                    
                    // Exchange right and center text alignments for iOS
                    if (attribute.alignment == NSTextAlignmentRight) {
                        
                        attribute.alignment = NSTextAlignmentCenter;
                    } else if (attribute.alignment == NSTextAlignmentCenter) {
                        
                        attribute.alignment = NSTextAlignmentRight;
                    }
                    
                    // Correct lineBreakMode for UILabel
                    if ([self.target isKindOfClass:[UILabel class]]) {
                        
                        UILabel *label = (UILabel *)self.target;
                        if (label.numberOfLines != 1) {
                            
                          attribute.lineBreakMode = NSLineBreakByWordWrapping;
                        }
                    }
                    attribute.allowsDefaultTighteningForTruncation = YES;
                    attributesForRange[@"paragraphStyle"] = [attribute description];
                }
            }
            textStyleDic[range] = attributesForRange;
        }
    }
    
    return textStyleDic;
}

- (UIView *)targetSuperview {
    
    if ([self.target respondsToSelector:@selector(superview)]) {
        
        return [self.target superview];
    }
    return nil;
}

- (CGRect)convertedFrame {
    
    CGRect superViewRect;
    UIView *superview = [self targetSuperview];
    CGRect parentFrame;// = [superview convertRect:self.target.frame toView:superview];
    if (superview) {
        
        superViewRect = superview.bounds;
        parentFrame = [superview convertRect:self.target.frame toView:superview];
    } else {
        
        parentFrame = self.target.frame;
    }
    
    return parentFrame;
}

- (CGRect)adjustFrameWithParentContentOffset:(CGRect)frame {
    
    UIView *superview = self.target.superview;
    if (superview && [superview isKindOfClass:[UIScrollView class]]) {
        
        UIScrollView *scrollView = (UIScrollView *)superview;
        if (CGPointEqualToPoint(scrollView.contentOffset, CGPointZero) == NO) {
            
            frame.origin.x -= scrollView.contentOffset.x;
            frame.origin.y -= scrollView.contentOffset.y;
        }
    }
    
    return frame;
}

- (void)grabFrameWithInfo:(NSMutableDictionary *)info {
    
    CGRect frameRelativeToWindow = [self.target convertRect:[self.target bounds] toView:nil];

    BOOL outOfBounds = (frameRelativeToWindow.origin.x > self.target.window.frame.size.width ||
                        frameRelativeToWindow.origin.y > self.target.window.frame.size.height ||
                        frameRelativeToWindow.origin.x + frameRelativeToWindow.size.width < 0 ||
                        frameRelativeToWindow.origin.y + frameRelativeToWindow.size.height < 0);
    if (self.isOnScreen && outOfBounds) {
        
        info[AOInfo.visible] =  @"0";
        self.isOnScreen = NO;
    } else if (!self.isOnScreen && !outOfBounds) {
        
        BOOL hidden = [self isTargetHidden];
        info[AOInfo.visible] =  [@(!hidden) stringValue];
        self.isOnScreen = YES;
    }
    if (![self targetSuperview]) {
        
        info[AOInfo.zIndex] = @([[[UIApplication sharedApplication] windows] indexOfObject:[self.target window]]);
    }
    
    CGRect convertedFrame = [self adjustFrameWithParentContentOffset:[self convertedFrame]];
    CGRect convertedVisibleFrame = [self convertedVisibleFrame];
    
    convertedFrame = [self.target osXRect:convertedFrame];
    
    info[AOInfo.visibleFrame] = NSStringFromCGRect(convertedVisibleFrame);
    info[AOInfo.frame] = NSStringFromCGRect(convertedFrame);
}

- (CGRect) convertedVisibleFrame {
    
    CGRect visibleRect = self.target.visibleRect;
//    if (self.target.isFlipped) {
//        CGFloat x = visibleRect.origin.x;
//        CGFloat y = self.target.frame.size.height - visibleRect.origin.y - visibleRect.size.height;
//        visibleRect.origin = CGPointMake(x, y);
//    }
    return visibleRect;
}

- (void)grabVisibleWithInfo:(NSMutableDictionary *)info {
    
    CGRect frame = self.target.bounds;
    
    if ([[self.target className] isEqualToString:@"GlyphButton"]) {
        
        [self grabTextStyleWithInfo:info];
    }
    
    if ([self.target isKindOfClass:[UITableView class]]) {
        
        UIScrollView *scrollView = (UIScrollView *)[self.target enclosingViewOfClassFromArray:@[[UIScrollView class]]];
        if (scrollView) {
            
            frame = scrollView.visibleRect;
        }
    }
    
    NSData *imageData = nil;
    
    if ([[self.target className] isEqualToString:@"Skype.ChatTextSelectionTextField"] ||
        [[self.target className] isEqualToString:@"MouseOverButton"                 ] ||
        [[self.target className] isEqualToString:@"CallPopoverMenuItemView"         ] ||
//        [[self.target className] isEqualToString:@"RCTText"                         ] ||
        [self.target isKindOfClass:[UITableView class]]) {
        
        imageData = [self.target.layer pdfDataWithDataConsumer];
    } else {
        
        id ex = nil;
        
        @try {
            
            NSArray *subviewsHiddenStatuses = [self.target hideSubviewsIfNeeded];
            
            NSTextStorage *textStorage;
//            NSString *text;
            if ([[self.target className] isEqualToString:@"RCTText"]) {
                textStorage = [self.target performSelectorWithName:@"textStorage" andParams:nil];
//                NSLog(@"===== %s: Text storage: %@", __FUNCTION__, textStorage);
                if (textStorage) {
//                    NSLog(@"===== %s: Emptying text storage...", __FUNCTION__);
//                    [self.target performSelectorWithName:@"setTextStorage:" andParams:@[[NSMutableAttributedString new]]];
                    [NSObject putValues:@[[NSTextStorage new]] withSelectorName:@"setTextStorage:" toObject:self.target];
//                    [self.target putValues:@[[NSMutableAttributedString new]] withSelectorName:@"setTextStorage:"];
//                    NSLog(@"===== %s: Emptying text storage - done.", __FUNCTION__);
                }
/*            } else if ([self.target isKindOfClass:[UILabel class]]) {
//                label = (UILabel *)self.target;
                text = [((UILabel *)self.target) text];
                [((UILabel *)self.target) setText:@""];*/
            }
            
            if ([self.target isKindOfClass:[UILabel class]] == NO) {
                imageData = [self.target.layer pdfData];
            }
            
            if (textStorage) {
                
//                NSLog(@"===== %s: Restoring text storage...", __FUNCTION__);
//                [self.target performSelectorWithName:@"setTextStorage:" andParams:@[textStorage]];
                [NSObject putValues:@[textStorage] withSelectorName:@"setTextStorage:" toObject:self.target];
//                NSLog(@"===== %s: Restoring text storage - done.", __FUNCTION__);
/*            } else if ([self.target isKindOfClass:[UILabel class]]) {
                [((UILabel *)self.target) setText:text];*/
            }
            
            [self.target showSubviewsIfNeeded:subviewsHiddenStatuses];
        } @catch (NSException *exception) {
            
            ex = exception;
        } @finally {
            
        }
    }
    
    if (imageData) {
        
        self.wasRendered = YES;
        [CHIInjectedAppManager writeObject:imageData path:[self.writePath stringByAppendingPathComponent:kPdfFileName]];
        [self commonVisibleGrabWithInfo:info];
    }
}

- (void)commonVisibleGrabWithInfo:(NSMutableDictionary *)info {
    
    if (self.wasRendered) {
        
        self.lastVisibleRenderFrameName = self.frameName;
        info[AOInfo.wasRendered] = @YES;
    }
}

- (void)targetDeallocNotification:(NSNotification *)notification {
    
    NSDictionary *userInfo = notification.userInfo;
    id object = userInfo[@"object"];
    if (object == self.target) {
        
        [self removeObserver];
        [self rip];
    }
}

- (void)rip {
    
    if (self.superViewPath) {
        
        [self createNewWritePath];
        [CHIInjectedAppManager writeObject:@{ @"visible" : @"0" }
                                      path:[self.writePath stringByAppendingPathComponent:kPlistFileName]
         ];
    }
}

- (void)deleteAONotification:(NSNotification *)notification {
    
    [self removeObserver];
    objc_setAssociatedObject(self.target, kAssociatedObjectName, nil, OBJC_ASSOCIATION_ASSIGN);
}

- (void)notificationFromServer:(NSNotification *)notification {
    
    self.frameName = notification.userInfo[kNotificationParamFrameName];
    
    @try {
        
        [self grabTarget];
    } @catch (NSException *exception) {
        
        NSLog(@"===== EXCEPTION in grabTarget (frame: %@): %@", self.frameName, exception);
    } @finally {
        
    }
}

- (void)removeObserver {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    for (NSString *key in self._keys) {
        
        [self.target removeObserver:self forKeyPath:key];
    }
    self._keys = nil;
}

- (void)dealloc {
    
//    NSLog(@"ao deallocated %@", self);
    if (self.sendNotificationToServerOnDealloc) {
        
        [[CHIAppClient sharedInstance] sendRequestType:kRequestTypeStatus
                                                  name:self.sendNotificationToServerOnDealloc[@"name"]
                                                params:self.sendNotificationToServerOnDealloc[@"params"]
                                            completion:nil];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.sendNotificationToServerOnDealloc = nil;
    self.projectRootPath = nil;
    self.targetName = nil;
    self.className = nil;
    self.superViewPath = nil;
    self.writePath = nil;
    self.frameName = nil;
    self.target = nil;
    self.lastVisibleRenderFrameName = nil;
    self.lastVisibleRenderObjectPath = nil;
    self.lastChangesFrameName = nil;
    self._isSelected = nil;
    self.createDate = nil;
//    self._addedTrakingArea = nil;
}

#pragma mark - Private methods

- (void)createNewWritePath {
    
    self.writePath = [[self.projectRootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", self.frameName]] stringByAppendingPathComponent:self.superViewPath];
//    NSLog(@"===== %s (%@):\nSuperview path: %@\nWrite path    : %@", __FUNCTION__, self.frameName, self.superViewPath ?: @"[NONE]", self.writePath);
    [CHIInjectedAppManager createDirectoryPath:self.writePath];
}

@end
