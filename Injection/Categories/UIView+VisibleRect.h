//
//  UIView+VisibleRect.h
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (VisibleRect)

@property (nonatomic, readonly) CGRect visibleRect;

@end
