//
//  CHIAppCommunication.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 24.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAppCommunication.h"
#import "NSObject+ClassName.h"
#import "NSObject+IPAddress.h"

@interface CHIAppCommunication ()

@property (nonatomic, strong) NSString *_lastRequestId;

@end

@implementation CHIAppCommunication

#pragma mark - Public methods

+ (instancetype)sharedInstance {
    return nil;
}

- (void)start {
    self._lastRequestId = nil;
    self.queue = [NSMutableArray array];
}

- (void)sendRequest:(CHIAppClientServerRequest *)request timeout:(double)timeout completion:(CHIRequestCompletion)completion {
    [self sendRequest:request timeout:timeout clientId:nil completion:completion];
}

- (void)sendRequest:(CHIAppClientServerRequest *)request timeout:(double)timeout clientId:(NSString *)clientId completion:(CHIRequestCompletion)completion {
    if (completion) {
        [self.queue addObject:@{ @"request"   : request,
                                 @"fireDate"  : [NSDate date],
                                 @"timeout"   : @(timeout),
                                 @"completion": completion
                                 }
         ];
        if (timeout > 0) {
            [self performSelector:@selector(checkTimeouted) withObject:nil afterDelay:timeout];
        }
    }
}

- (void)sendRequestType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params timeout:(double)timeout clientId:(NSString *)clientId completion:(CHIRequestCompletion)completion {
    CHIAppClientServerRequest *request = [CHIAppClientServerRequest requestWithType:type name:name params:params];
    [self sendRequest:request timeout:timeout clientId:clientId completion:completion];
}

- (void)sendRequestType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params timeout:(double)timeout completion:(CHIRequestCompletion)completion {
    [self sendRequestType:type name:name params:params timeout:timeout clientId:nil completion:completion];
}

- (void)sendRequestType:(NSString *)type name:(NSString *)name params:(NSDictionary *)params completion:(CHIRequestCompletion)completion {
    [self sendRequestType:type name:name params:params timeout:0 completion:completion];
}

- (NSString *)ipAddress {
//    NSArray *addresses = [NSHost currentHost].addresses;
    NSString *ipAddr = [NSObject ipAddress] ?: @"localaddress";
//    for (NSString *address in addresses) {
//        if ([address hasPrefix:@"192.168"]) {
//            ipAddr = address;
//        }
//    }
    ipAddr = [[[ipAddr stringByReplacingOccurrencesOfString:@"." withString:@""] stringByReplacingOccurrencesOfString:@":" withString:@""] stringByReplacingOccurrencesOfString:@"%" withString:@""];
    return ipAddr;
}

#pragma mark - Private methods

- (void)checkTimeouted {
    
    for (NSInteger i = self.queue.count - 1; i >= 0; i--) {
        
        NSDictionary *requestRecord = self.queue[i];
        NSDate *fireDate = requestRecord[@"fireDate"];
        CHIAppClientServerRequest *request = requestRecord[@"request"];
        double timeout = [requestRecord[@"timeout"] doubleValue];
        if (timeout > 0) {
            NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:fireDate];
            if (timeInterval >= timeout) {
                CHIRequestCompletion completion = requestRecord[@"completion"];
                if (completion) {
                    for (NSString *client in request.clients) {
                        completion(nil, [NSError errorWithDomain:@"Timeout error" code:0 userInfo:@{@"timeout":@(timeout), @"client":client}]);
                    }
                }
                [self.queue removeObjectAtIndex:i];
            }
        }
    }
}

- (void)checkRequest:(id)theData fromClientId:(NSString *)clientId {
    
//    NSDictionary *data = [theData objectFromJSONString];
    NSData *jsonData = [theData dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (error) {
        
        NSLog(@"ERROR: %@", error.localizedDescription);
        return;
    }
    if (!data.count) {
        
        NSLog(@"ERROR: Dictionary taken from the JSON string is empty.");
        return;
    }
    id requestClass = data[@"class"];
    if (![requestClass isEqualToString:[CHIAppClientServerRequest className]]) {
        
        NSLog(@"ERROR: Not expected value of the \"class\" request parameter (%@).", requestClass);
        return;
    }
    CHIAppClientServerRequest *request = [CHIAppClientServerRequest requestFromJSONString:theData];
    if ([request.guid isEqualToString:self._lastRequestId]) {
        
        return;
    }
    
    self._lastRequestId = request.guid;
    
    BOOL found = NO;
    for (NSInteger i = self.queue.count - 1; i >= 0; i--) {
        
        NSDictionary *requestRecord = self.queue[i];
        CHIAppClientServerRequest *requestFromQueue = requestRecord[@"request"];
        NSString *requestGuid = request.guid;
        if (request.params[@"senderId"]) {
            
            requestGuid = request.params[@"senderId"];
        }
        if ([requestGuid isEqualToString:requestFromQueue.guid]) {
            
            CHIRequestCompletion completion = requestRecord[@"completion"];
            if (completion) {
                
                for (NSInteger i = requestFromQueue.clients.count - 1; i >= 0; i--) {
                    
                    NSString *client = requestFromQueue.clients[i];
                    if ([client isEqualToString:clientId]) {
                        
                        [requestFromQueue.clients removeObjectAtIndex:i];
                        completion(data, data[@"error"]);
                        found = YES;
                    }
                }
            }
            if (requestFromQueue.clients.count == 0) {
                
                [self.queue removeObjectAtIndex:i];
            }
            break;
        }
    }
    
    [self didReceiveData:data];
}

#pragma mark - Base method (for overriding in descendants)

- (void)didReceiveData:(NSDictionary *)theData {
    
}

#pragma mark - ThoMoServerDelegate methods

- (void)client:(ThoMoClientStub *)theClient didConnectToServer:(NSString *)aServerIdString {
    
    NSLog(@"===== %s: %@", __FUNCTION__, aServerIdString);
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        NSLog(@"===== %s: %@ - Searching for the topmost VC", __FUNCTION__, aServerIdString);
//        UIViewController *vc = [UIViewController topmostViewControllerWithRootViewController:nil];
//        if (vc) {
//            NSLog(@"===== %s: %@ - The topmost VC found", __FUNCTION__, aServerIdString);
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connected to the Recorder!"
//                                                                           message:[NSString stringWithFormat:@"Server ID: %@", aServerIdString]
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
//                                                      style:UIAlertActionStyleDefault
//                                                    handler:^(UIAlertAction * _Nonnull action)
//                              {
////                                  [CHIInjectedAppManager start];
//                              }]
//             ];
//            [vc presentViewController:alert animated:YES completion:^{
//                NSLog(@"===== %s: %@ - Alert displayed", __FUNCTION__, aServerIdString);
//            }];
//        }
//    });
}

- (void)client:(ThoMoClientStub *)theClient didDisconnectFromServer:(NSString *)aServerIdString errorMessage:(NSString *)errorMessage {
    
    NSLog(@"===== %s: %@ (%@)", __FUNCTION__, aServerIdString, errorMessage);
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        UIViewController *vc = [UIViewController topmostViewControllerWithRootViewController:nil];
//        if (vc) {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Disconnected from the Recorder!"
//                                                                           message:[NSString stringWithFormat:@"Server ID: %@\n%@", aServerIdString, errorMessage]
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
//                                                      style:UIAlertActionStyleDefault
//                                                    handler:^(UIAlertAction * _Nonnull action)
//                              {
//                                  //                                  [CHIInjectedAppManager start];
//                              }]
//             ];
//            [vc presentViewController:alert animated:YES completion:nil];
//        }
//    });
}

- (void)netServiceProblemEncountered:(NSString *)errorMessage onClient:(ThoMoClientStub *)theClient {
    
    NSLog(@"===== %s: %@", __FUNCTION__, errorMessage);
}

- (void)clientDidShutDown:(ThoMoClientStub *)theClient {
    
    NSLog(@"===== %s", __FUNCTION__);
}

- (void)client:(ThoMoClientStub *)theClient didReceiveData:(id)theData fromServer:(NSString *)aServerIdString {
    
    [self checkRequest:theData fromClientId:aServerIdString];
}

@end
