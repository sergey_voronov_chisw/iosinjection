//
//  NSObject+ConvertPoints.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/4/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+ConvertPoints.h"

typedef UIView* (*SuperviewMethod)(id, SEL);
typedef CGRect  (*FrameMethod    )(id, SEL);

@implementation NSObject (ConvertPoints)

#pragma mark - Public methods

- (CGPoint)osXPoint:(CGPoint)point {
    
    CGPoint result = point;
    
    CGRect parentFrame = [self parentFrame];
    if (CGRectEqualToRect(parentFrame, CGRectZero) == NO) {
        
        result.y = parentFrame.size.height - point.y;
//    } else {
//        
//        result.origin.y = [[self class] windowSize].height;
    }
    
    return result;
}

- (CGRect)osXRect:(CGRect)frame {
    
    CGRect result = frame;
    
    CGRect parentFrame = [self parentFrame];
    if (CGRectEqualToRect(parentFrame, CGRectZero) == NO) {
        
        result.origin.y = parentFrame.size.height - frame.origin.y - frame.size.height;
//    } else {
//        
//        result.origin.y = [[self class] windowSize].height;
    }
    
    return result;
}

#pragma mark - Private method

- (CGRect)parentFrame {
    
    CGRect parentFrame = CGRectZero;
    SEL selector = NSSelectorFromString(@"superview");
    if ([self respondsToSelector:selector]) {
        
        SuperviewMethod superviewMethod = (SuperviewMethod)[self methodForSelector:selector];
        UIView *superview = (superviewMethod)(self, selector);
        if (superview) {
            
            selector = NSSelectorFromString(@"frame");
            if ([self respondsToSelector:selector]) {
                
                FrameMethod frameMethod = (FrameMethod)[superview methodForSelector:selector];
                parentFrame = (frameMethod)(superview, selector);
            }
        }
    }
    return parentFrame;
}

+ (CGSize)windowSize {
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    UIApplication *app = [UIApplication sharedApplication];
    
    CGSize windowSize;
    
    if (app.statusBarOrientation == UIInterfaceOrientationPortrait ||
        app.statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        
        windowSize = screenSize;
    } else {
        
        windowSize = CGSizeMake(screenSize.height, screenSize.width);
    }
    
    return windowSize;
}

@end
