//
//  NSBundle+Fonts.h
//  iOSInjection
//
//  Created by userMacBookPro on 8/29/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Fonts)

- (NSArray *)fonts;

@end
