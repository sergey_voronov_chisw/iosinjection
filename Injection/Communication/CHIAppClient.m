//
//  CHIAppClient.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 23.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAppClient.h"
#import "Constants.h"

static CHIAppClient *_instance;

@interface CHIAppClient ()

@property (nonatomic, strong) ThoMoClientStub *_clientStub;

@end

@implementation CHIAppClient

#pragma mark - Public method

+ (instancetype)sharedInstance {
    
    if (!_instance) {
        
        _instance = [[CHIAppClient alloc] init];
        [_instance start];
    }
    
    return _instance;
}

#pragma mark - Inherited methods

- (void)start {
    
    NSLog(@"===== %s", __FUNCTION__);
    [super start];
    
    NSString *protocolIdentifier = [[NSUserDefaults standardUserDefaults] stringForKey:@"ProtocolIdentifier"];
    if (!protocolIdentifier.length) {
        protocolIdentifier = @"test";
    }
    self._clientStub = [[ThoMoClientStub alloc] initWithProtocolIdentifier:protocolIdentifier/*[self ipAddress]*/];
    [self._clientStub setDelegate:(id)self];
    NSLog(@"===== %s: Client Stub starting with protocol identifier: %@...", __FUNCTION__, protocolIdentifier);
    [self._clientStub start];
}

- (void)sendRequest:(CHIAppClientServerRequest *)request
            timeout:(double)timeout
           clientId:(NSString *)clientId
         completion:(CHIRequestCompletion)completion {
    
    [super sendRequest:request timeout:timeout clientId:clientId completion:completion];

    [self._clientStub sendToAllServers:[request JSONString]];
}

- (void)didReceiveData:(NSDictionary *)data {
    
    [[NSNotificationCenter defaultCenter] postNotificationName: kClientNotification
                                                        object: nil
                                                      userInfo: @{ kNotificationUserInfoKeyRequest: data }];
}

@end
