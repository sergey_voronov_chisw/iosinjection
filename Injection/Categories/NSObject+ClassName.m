//
//  NSObject+ClassName.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "NSObject+ClassName.h"

@implementation NSObject (ClassName)

- (NSString *)className {
    return NSStringFromClass([self class]);
}

@end
