//
//  CHIWindowAnalyzer.h
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 19.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

static dispatch_group_t grabGroup;

@interface CHIWindowAnalyzer : NSObject

- (void)processWithFrameName:(NSString *) frameName andGrab:(NSString *)grab;
- (void)resetWithStorePath:(NSString *)path;
- (void)stopRecording;

@end
