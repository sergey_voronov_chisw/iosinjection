//
//  NSData+CheckSum.h
//  iOSInjection
//
//  Created by Sergey Voronov on 8/21/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (CheckSum)

- (NSUInteger)checksum;

@end
