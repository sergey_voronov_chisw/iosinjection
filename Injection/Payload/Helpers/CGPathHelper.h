//
//  CGPathHelper.h
//  iOSInjection
//
//  Created by userMacBookPro on 9/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface CGPathHelper : NSObject

@property (nonatomic) CGPathRef pathRef;

- (instancetype)initWithPathRef:(CGPathRef)pathRef;
- (NSArray *)pathArray;
- (BOOL)setupPathRefWithArray:(NSArray *)array;

@end
