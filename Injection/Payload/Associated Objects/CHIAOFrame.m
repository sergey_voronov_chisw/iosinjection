//
//  CHIAOFrame.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 07.10.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAOFrame.h"
#import "Constants.h"
#import "UIView+VisibleRect.h"

@implementation CHIAOFrame

- (void)setupObserver {
    self.wasChanges = kChangeFrame;
}

- (BOOL)shouldGrab {
    CGRect frame = [self convertedFrame];
    
    if (!CGRectEqualToRect(self.lastFrame, frame)) { // Если фрейм изменился (!Equal)
        self.wasChanges = self.wasChanges | kChangeFrame;
        self.lastFrame = frame;
    }
    
    if (!CGRectEqualToRect(self.target.visibleRect, self.lastVisibleRect)) {
        self.wasChanges = self.wasChanges | kChangeFrame;
        self.lastVisibleRect = self.target.visibleRect;
    }
    
    return self.wasChanges != kChangeNone;
}

@end
