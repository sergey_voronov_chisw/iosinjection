//
//  UIView+Hierarchy.h
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Hierarchy)

- (UIView *)enclosingViewOfClassFromArray:(NSArray<Class> *)classes;
- (UIView *)enclosingViewOfClassName:(NSString *)className;
- (BOOL)isKindOfClasses:(NSArray<Class> *)classes;
- (BOOL)isOfClassNames:(NSArray<NSString *> *)classNames;

- (NSString *)hierarchyPath;

- (NSArray *)hideSubviewsIfNeeded;
- (void)showSubviewsIfNeeded:(NSArray<NSNumber *> *)subviewsHiddenStatuses;

- (void)DEBUG_logSuperviews;
- (void)DEBUG_logSubviews;

@end
