//
//  NSData+CheckSum.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/21/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "NSData+CheckSum.h"

@implementation NSData (CheckSum)

- (NSUInteger)checksum {
    NSUInteger
        result = 0,
        length = self.length;
    const char *pos = self.bytes;
    
    for (NSUInteger i = 0; i < length; ++i) {
        result += *pos;
    }
    
    return result;
}

@end
