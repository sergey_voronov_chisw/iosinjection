//
//  UIView+VisibleRect.m
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIView+VisibleRect.h"

@implementation UIView (VisibleRect)

- (CGRect)visibleRect {
    CGRect result;
    if (self.superview) {
        result = CGRectIntersection(self.frame, self.superview.bounds);
    } else {
        result = self.frame;
    }
    return result;
}

@end
