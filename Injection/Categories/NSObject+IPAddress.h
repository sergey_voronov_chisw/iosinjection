//
//  NSObject+IPAddress.h
//  iOSInjection
//
//  Created by Sergey Voronov on 7/7/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (IPAddress)

+ (NSString *)ipAddress;

@end
