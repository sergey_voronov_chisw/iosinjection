//
//  UILabel+AttributedTitle.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/3/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UILabel+AttributedTitle.h"

@implementation UILabel (AttributedTitle)

- (NSAttributedString *)attributedTitle {
    NSDictionary *attributes = @{ NSFontAttributeName           : self.font,
                                  NSForegroundColorAttributeName: self.textColor
                                  };
    NSAttributedString *result = [[NSAttributedString alloc] initWithString:self.text attributes:attributes];
    return result;
}

@end
