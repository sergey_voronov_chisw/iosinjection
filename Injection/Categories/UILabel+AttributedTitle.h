//
//  UILabel+AttributedTitle.h
//  iOSInjection
//
//  Created by Sergey Voronov on 8/3/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (AttributedTitle)

- (NSAttributedString *)attributedTitle;

@end
