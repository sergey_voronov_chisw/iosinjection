//
//  NSObject+ClassName.h
//  iOSInjection
//
//  Created by Sergey Voronov on 7/6/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ClassName)

- (NSString *)className;

@end
