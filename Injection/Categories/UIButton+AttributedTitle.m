//
//  UIButton+AttributedTitle.m
//  iOSInjection
//
//  Created by Sergey Voronov on 8/3/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#import "UIButton+AttributedTitle.h"

@implementation UIButton (AttributedTitle)

- (NSAttributedString *)attributedTitle {
    NSAttributedString *result;
    if (self.titleLabel.text.length) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = self.titleLabel.textAlignment;
        NSDictionary *attributes = @{ NSFontAttributeName           : self.titleLabel.font,
                                      NSForegroundColorAttributeName: self.titleLabel.textColor,
                                      NSParagraphStyleAttributeName : paragraphStyle
                                      };
        result = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:attributes];
    } else {
        result = [NSAttributedString new];
    }
    return result;
}

@end
