//
//  CHIAOButton.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 09.08.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIAOButton.h"
#import "CHIUIUtilities.h"
#import "Constants.h"
#import "NSObject+ClassName.h"
#import "UIButton+OsXTitleRectForBounds.h"
#import "UIButton+AttributedTitle.h"

//#import "UIView+Hierarchy.h"

@interface CHIAOButton ()

@property (nonatomic) BOOL _highlightedHash;

@end

@implementation CHIAOButton

//- (instancetype)initWithTargetObject:(UIView *)object path:(NSString *)path {
//    self = [super initWithTargetObject:object path:path];
//    
//    if (self) {
//    }
//    
//    return self;
//}
//
- (void)resetWithPath:(NSString *)path {
    
    [super resetWithPath:path];
    
    self._highlightedHash = [(id)self.target isHighlighted];
    CGRect rect = [/*[*/(UIButton *)self.target /*cell]*/ titleRectForBounds:self.target.bounds];
    if (!CGRectEqualToRect(rect, CGRectZero)) {
        
        self.margins = CGRectMake(rect.origin.x,
                                  rect.origin.y,
                                  self.target.frame.size.width  - rect.size.width  - rect.origin.x,
                                  self.target.frame.size.height - rect.size.height - rect.origin.y
                                  );
    }
}

- (void)setupObserver {
    
    [super setupObserver];
    
    [self addObserverForKeyPath:@"title"];
    [self addObserverForKeyPath:@"attributedTitle"];
    [self addObserverForKeyPath:@"image"];
    [self addObserverForKeyPath:@"alternateImage"];
    [self addObserverForKeyPath:@"bordered"];
    [self addObserverForKeyPath:@"transparent"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    
    if ([keyPath isEqualToString:@"attributedTitle"] || [keyPath isEqualToString:@"title"]) {
        
        self.wasChanges |= kChangeText;
    } else if ([keyPath isEqualToString:@"bordered"] || [keyPath isEqualToString:@"transparent"]) {
        
        self.wasChanges |= kChangeVisibleState;
    }
}

- (BOOL)shouldGrab {
    
    if ([(id)self.target isHighlighted] != self._highlightedHash) {
        
        self._highlightedHash = [(id)self.target isHighlighted];
        self.wasChanges |= kChangeVisibleState | kChangeText;
    }
    return [super shouldGrab];
}

- (void)grabTextWithInfo:(NSMutableDictionary *)info {
    
    NSString *title = [(id)self.target currentTitle];
    
//    if (CGSizeEqualToSize(self.target.frame.size, CGSizeMake(305, 44/*24, 24*/))) {
//        NSLog(@"===== %s: %@ (%@)", __FUNCTION__, title, ((UIButton *)self.target).currentImage);
//        [self.target DEBUG_logSubviews];
//    }
    if (!title) {
        
        title = @"";
    }
    
    info[AOInfo.text] = title;
}

- (void)grabTextStyleWithInfo:(NSMutableDictionary *)info {
    
//    if ([[self.target className] isEqualToString:@"ParticipantPillControl"]) {
//        NSLog(@"");
//    }
    
    NSAttributedString *attributedTitle = [((UIButton *)self.target) attributedTitle];
    if (![[attributedTitle string] isEqualToString:@""]) {
        
        info[AOInfo.textStyle] = [self textStyleForAttributedString:attributedTitle];
    }
    info[AOInfo.margins] = @[@(self.margins.origin.x),
                             @(self.margins.origin.y),
                             @(self.margins.size.width),
                             @(self.margins.size.height)];
}

@end
