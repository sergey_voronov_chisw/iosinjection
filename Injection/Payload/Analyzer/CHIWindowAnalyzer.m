//
//  CHIWindowAnalyzer.m
//  LayeredScreenRecorder
//
//  Created by Andrew Danileyko on 19.05.16.
//  Copyright © 2016 CHI Software. All rights reserved.
//

#import "CHIWindowAnalyzer.h"
#import "CHIUIUtilities.h"
#import "Constants.h"
#import "CHIAppClientServerRequest.h"
#import "CHIAssociatedObject.h"
#import "CHIAppClient.h"
#import "CHIAOTextField.h"
#import "CHIAOView.h"
#import "CHIAOButton.h"
#import "CHIAOFrame.h"
#import "CHIAODesktopIconTitle.h"
#import "CHIAOQTView.h"
#import "CHIAOImage.h"
#import "CHIAOStub.h"
#import "NSObject+ClassName.h"
#import "NSObject+Selectors.h"
#import "UIView+VisibleRect.h"
#import "UIView+Hierarchy.h"
#import <objc/runtime.h> // for objc_getAssociatedObject()

static NSString * const kUIScrollViewMirrorView = @"UIScrollViewMirrorView";

@interface CHIWindowAnalyzer ()

@property (nonatomic, strong) NSString *_path;
@property (nonatomic) int _aoCount;
@property (nonatomic, strong) NSString *_frameName;
@property (nonatomic) int _frameAOCount;
@property (nonatomic) int _frameGrabCount;
@property (nonatomic, strong) NSDate *_frameStartDate;
@property (nonatomic) NSDate *_waitDate;

@end

@implementation CHIWindowAnalyzer

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        [NSObject swizzleOrigClass:[UIView class]
                           origSel:NSSelectorFromString(@"dealloc")
                        patchClass:[self class]
                          patchSel:@selector(swizzledDealloc)
         ];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didFinishGrabNotification:)
                                                     name:kAOGrabFinishNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(targetDeallocNotification:)
                                                     name:[NSString stringWithFormat:kNotificationUIViewDealloc]
                                                   object:nil];
        self._waitDate = nil;
        self._frameName = nil;
    }
    
    return self;
}

// Injected app's UIView DEALLOC
- (void)swizzledDealloc {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUIViewDealloc
                                                        object:nil
                                                      userInfo:@{@"object":self}];
    [self swizzledDealloc]; // call origin selector (swizzled before)
}

- (void)resetWithStorePath:(NSString *)path {
    
    self._path = path;
    self._waitDate = nil;
    self._frameName = nil;
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationResetAO
//                                                        object:nil
//                                                      userInfo:@{@"path":self._path}];
}

- (void)stopRecording {
    
    self._waitDate = nil;
    self._frameName = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDeleteAO
                                                        object:nil
                                                      userInfo:@{}];
}

- (void)processWithFrameName:(NSString *) frameName andGrab:(NSString *)grab {
    
    if (self._frameName) {
        
        [[CHIAppClient sharedInstance] sendRequestType: kRequestTypeStatus
                                                  name: kRequestNameAnalyzeIgnored
                                                params: @{ RequestParams.frameName: self._frameName }
                                            completion: nil];
        NSLog(@"===== GRAB CANCELED 1 (frameName is not empty)");
        return;
    }
    
    if (self._waitDate) {
        
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:self._waitDate];
        if (time < 0.3) {
            
            [[CHIAppClient sharedInstance] sendRequestType: kRequestTypeStatus
                                                      name: kRequestNameAnalyzeIgnored
                                                    params: @{ RequestParams.frameName: @"wait" }
                                                completion: nil];
            NSLog(@"===== GRAB CANCELED 2 (wait time < 0.3)");
            return;
        }
        self._waitDate = nil;
    }

    self._frameName = frameName;
    self._aoCount = 0;
    
//    NSDate *startDate = [NSDate date];
    
    id statusBarWindow = [[UIApplication sharedApplication] performSelectorWithName:@"statusBarWindow" andParams:nil];
    NSArray *windows = [[UIApplication sharedApplication].windows arrayByAddingObject:statusBarWindow];
    
    [windows enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIWindow *window = obj;
        if (window.isHidden == NO) {
            if (window/*.contentView*/) {
//                NSString *identifier = [NSBundle mainBundle].bundleIdentifier;
                int canGrab = 1;
//                if ([identifier isEqualToString:@"com.apple.finder"] && ![NSStringFromClass([window class]) isEqualToString:@"TDesktopWindow"]) {
//                    canGrab = 0;
//                }
                if ([NSStringFromClass([window class]) hasPrefix:@"Q"]) {
                    canGrab = 2;
                }
                switch (canGrab) {
                    case 1:
                        [self analyzeSubviewsForView:window/*.contentView.superview*/];
                        break;
                    case 2:
                        [self addAOForTarget:window];
                        break;
                    default:
                        break;
                }
            }
        }
    }];

    NSLog(@"GRAB GOING TO CALL");
    [self grab:grab];
}

- (void)addAOForTarget:(id)target {
    
    id obj = objc_getAssociatedObject(target, kAssociatedObjectName);
    if (obj == nil) {
        
        UIView *incorrectRenderedRootObject =
            [target enclosingViewOfClassFromArray:[[self class] incorrectRenderedClasses]];
        if (!incorrectRenderedRootObject) {
            
            incorrectRenderedRootObject =
                [target enclosingViewOfClassFromArray:[[self class] incorrectRenderedClassNames]];
        }
        
        Class aoClass;
        if (incorrectRenderedRootObject) {
            
            aoClass = [CHIAOStub class];
        } else {
            
            aoClass = [[self class] aoClassForTarget:target];
        }
        
        id ao = nil;
        if (aoClass) {
            
            ao = [[aoClass alloc] initWithTargetObject:target path:self._path];
            CHIAssociatedObject *rogue = ao;
            rogue.zIndex = [[[((UIView *)target) superview] subviews] indexOfObject:target];
        }
        if (ao) {
            
            self._aoCount++;
        }
    } else {
        
        self._aoCount++;
    }
}

+ (Class)aoClassForTarget:(id)target {
    
    if ([[target className] hasPrefix:@"Q"]) {
        
        return [CHIAOQTView class];
    }
    
    if ([[target className] isEqualToString:kUIScrollViewMirrorView]) {
        
        return nil;
    }
    
    if ([target bounds].size.height == 0 || [target bounds].size.width == 0) {
        
        return nil;
    }
    
    if ([target isKindOfClasses:[self incorrectRenderedClasses   ]] ||
        [target isOfClassNames :[self incorrectRenderedClassNames]]) {
        
        return [CHIAOImage class];
    }
    
    if ([target isMemberOfClass:[UIScrollView class]]) {
            
        return [CHIAOFrame class];
    }
    
    if ([[target className] isEqualToString:@"TDesktopTitleBubbleView"]) {
        
        return [CHIAODesktopIconTitle class];
    }
    
    // For iOS Skype
    if ([target isKindOfClass:[UITextField class]] ||
        [target isKindOfClass:[UITextView class]] ||
        [target isKindOfClass:[UILabel class]] ||
        [[target className] isEqualToString:@"RCTTextField"] ||
        [[target className] isEqualToString:@"RCTText"] ||
        [[target className] isEqualToString:@"RCTUITextView"]) {
        
        return [CHIAOTextField class];
    }
    
    if ([target isKindOfClass:[UIImageView class]] ||
        [target isKindOfClass:[UITableView class]]) {
        
        return [CHIAOView class];
    }
    
    if ([target isKindOfClass:[UIButton class]]) {
        
        return [CHIAOButton class];
    }
    
    if (![target isKindOfClass:[UIControl class]]) {
        
        return [CHIAOView class];
    }
    
    return [CHIAssociatedObject class];
}

+ (NSArray<Class>*)incorrectRenderedClasses {
    
    return @[[UIStepper class], [UISwitch class]];
}

+ (NSArray<NSString *>*)incorrectRenderedClassNames {
    
    return @[@"IGTabBarButton"]; // Instagram
}

- (void)analyzeSubviewsForView:(UIView *)superView {
    
    [self addAOForTarget:superView];
    
    if (!(//[superView isKindOfClass:[UIButton class]] ||
          [superView isKindOfClass:[UITextField class]] ||
          [superView.className isEqualToString:kUIScrollViewMirrorView])) {
        
        for (int i = 0; i < superView.subviews.count; i++) {
            
            UIView *view = superView.subviews[i];
            if (!CGRectEqualToRect(view.visibleRect, CGRectZero)) {
                
                [self analyzeSubviewsForView:view];
            }
        }
    }
}

- (void)didFinishGrabNotification:(NSNotification *)notification {
    
    self._frameGrabCount ++;
//    NSLog(@"ASSOСIATEDOBJECT framename : %@  finished grab %d / %d", self._frameName, self._frameGrabCount, self._frameAOCount);
    
    if (self._frameAOCount == self._frameGrabCount) {
        
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:self._frameStartDate];
        NSString *frameName = self._frameName;
        self._frameName = nil;
       
        [[CHIAppClient sharedInstance] sendRequestType: kRequestTypeStatus
                                                  name: kRequestNameRoguesFinishGrab
                                                params: @{ RequestParams.frameName: frameName,
                                                           RequestParams.time     : @(time)
                                                           }
                                            completion:nil];
        
    }
}

- (void)targetDeallocNotification:(NSNotification *)notification {
    
    self._waitDate = [NSDate date];
}

- (void)grab:(NSString *)grab {
    
    BOOL all = [[grab lowercaseString] isEqualToString:@"all"];
    self._frameAOCount = all ? self._aoCount : 1;
    self._frameGrabCount = 0;
    self._frameStartDate = [NSDate date];
//    NSLog(@"===== ASSOCIATED OBJECT grab target notification posted");
    [[NSNotificationCenter defaultCenter] postNotificationName: kGrabTargetAllNotification
                                                        object: nil
                                                      userInfo: @{ kNotificationParamFrameName: self._frameName }
     ];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
